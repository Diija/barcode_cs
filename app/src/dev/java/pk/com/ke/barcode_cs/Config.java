package pk.com.ke.barcode_cs;

import okhttp3.logging.HttpLoggingInterceptor;

public class Config {

    public static final boolean IS_LOG_ALLOWED = true;

    private static final String URL = "fioridev.ke.com.pk";
    public static final String BASE_URL = "https://" + URL + ":44200";

    public static String FONT_NAME = "PT_Sans-Web-Regular.ttf";

    public static boolean triggerPosting = true;

    public static HttpLoggingInterceptor.Level LOG_LEVEL = HttpLoggingInterceptor.Level.BODY;

    public static final String CERTIFICATE = "-----BEGIN CERTIFICATE-----\n" +
            "MIICXDCCAcUCCAogGAInCSABMA0GCSqGSIb3DQEBBQUAMHMxCzAJBgNVBAYTAkRF\n" +
            "MRwwGgYDVQQKExNTQVAgVHJ1c3QgQ29tbXVuaXR5MRMwEQYDVQQLEwpTQVAgV2Vi\n" +
            "IEFTMRQwEgYDVQQLEwtJMDAyMDIzNzM1MDEbMBkGA1UEAxMSZmlvcmlkZXYua2Uu\n" +
            "Y29tLnBrMB4XDTE4MDIyNzA5MjAwMVoXDTM4MDEwMTAwMDAwMVowczELMAkGA1UE\n" +
            "BhMCREUxHDAaBgNVBAoTE1NBUCBUcnVzdCBDb21tdW5pdHkxEzARBgNVBAsTClNB\n" +
            "UCBXZWIgQVMxFDASBgNVBAsTC0kwMDIwMjM3MzUwMRswGQYDVQQDExJmaW9yaWRl\n" +
            "di5rZS5jb20ucGswgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBANG0IM09s4gG\n" +
            "FxrYLwTj9eDrCX+ugy7pWr50iM7MiBd9eQVLR8Uj/UdPzPbeVFOCkiqNJEwyA6Gw\n" +
            "XNEODHZFFYY8n2dimefGmCePCqBZ2Yz84WNCd+r1KsX8pviTxNNsxQ4REYkslmAl\n" +
            "+/oPd9IYwZ/qq0rQGliek+0J++1SY7D/AgMBAAEwDQYJKoZIhvcNAQEFBQADgYEA\n" +
            "irYq/J76tv8pOWREzBdMij/NNd9eAxesU1j8OGqU+G7brfFlYV8guyVpDskuS9my\n" +
            "Hys8ac1M5zComMszeFYyJh5LiNNBbchwigjEq7cVXuWUe5MS7wiTr5Ex954zl0xE\n" +
            "uP9hr/N91iMZh+fMiq0b6NspFXVKV7EPWVRHVvdoCA4=\n" +
            "-----END CERTIFICATE-----\n";


    public static class SP {
        public static String USER_NAME = "djasodij128u";
        public static String PASSWORD = "aklsjndcd128p";
        public static String IMEI = "alsdnc1092f12i";
        public static String WAREHOUSE_NUMBER = "waksjndkj12id2w";
        public static String STORAGE_TYPE = "askdj12u8fnd2s";
    }



}

package pk.com.ke.barcode_cs;

import okhttp3.logging.HttpLoggingInterceptor;

public class Config {

    public static final boolean IS_LOG_ALLOWED = false;

    // TODO Change Server Name, Port # and Certificate also
    private static final String URL = "fioridev.ke.com.pk";
    public static final String BASE_URL = "https://" + URL + ":44200";

    public static String FONT_NAME = "PT_Sans-Web-Regular.ttf";

    public static HttpLoggingInterceptor.Level LOG_LEVEL = HttpLoggingInterceptor.Level.NONE;

    public static final String CERTIFICATE = "-----BEGIN CERTIFICATE-----\n" +
            "\n" +
            "-----END CERTIFICATE-----\n";


    public static class SP {
        public static String USER_NAME = "djasodij128u";
        public static String PASSWORD = "aklsjndcd128p";
        public static String IMEI = "alsdnc1092f12i";
        public static String WAREHOUSE_NUMBER = "waksjndkj12id2w";
        public static String STORAGE_TYPE = "askdj12u8fnd2s";

    }

}

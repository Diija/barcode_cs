package pk.com.ke.barcode_cs;

import okhttp3.logging.HttpLoggingInterceptor;

public class Config {

    public static final boolean IS_LOG_ALLOWED = true;

    // TODO Change Server Name, Port # and Certificate also
    private static final String URL = "fioriqacopy.ke.com.pk";
    public static final String BASE_URL = "https://" + URL + ":44300";

    public static String FONT_NAME = "PT_Sans-Web-Regular.ttf";

    public static HttpLoggingInterceptor.Level LOG_LEVEL = HttpLoggingInterceptor.Level.BODY;

    public static final String CERTIFICATE = "-----BEGIN CERTIFICATE-----\n" +
            "MIICVDCCAb0CCAogGAMnEUMBMA0GCSqGSIb3DQEBBQUAMG8xCzAJBgNVBAYTAkRF\n" +
            "MRwwGgYDVQQKExNTQVAgVHJ1c3QgQ29tbXVuaXR5MRMwEQYDVQQLEwpTQVAgV2Vi\n" +
            "IEFTMREwDwYDVQQLEwhJSU5JVElBTDEaMBgGA1UEAxMRZmlvcmlxYS5rZS5jb20u\n" +
            "cGswHhcNMTgwMzI3MTE0MzAxWhcNMzgwMTAxMDAwMDAxWjBvMQswCQYDVQQGEwJE\n" +
            "RTEcMBoGA1UEChMTU0FQIFRydXN0IENvbW11bml0eTETMBEGA1UECxMKU0FQIFdl\n" +
            "YiBBUzERMA8GA1UECxMISUlOSVRJQUwxGjAYBgNVBAMTEWZpb3JpcWEua2UuY29t\n" +
            "LnBrMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD0opzobTJ6HWnfVrwDCZo1\n" +
            "I1//Ifq4jY+r4b8QctZbM9nY9xfTisvQrqqFP/y3y0biLblgpIPhdAxfpk9D2MwM\n" +
            "VwuiH1tzgbDQj3A1Gf7htOK7/CYh2BTp2tFurxp3zVWaEjJVh5haM4xkZyz/aBRp\n" +
            "6iA3RKTeQw6ndzve1+EgCQIDAQABMA0GCSqGSIb3DQEBBQUAA4GBAGZIJcof+B/8\n" +
            "0PS7rkC88qCNJS9uTuiE9kNf3yimIX1YMsglH2F7Rmm5MVL/YG+RX7Xe0p5cTZIc\n" +
            "QioaVFC5Xylk7d8WGgPZs74V2UdWrP3RvJ3xKJSxyW4coWEB0nVZvDKSSP2PeGG9\n" +
            "OvUXASRTvxV62VRPbZhJK3TUL6LkQXAD\n" +
            "-----END CERTIFICATE-----\n";


    public static class SP {
        public static String USER_NAME = "djasodij128u";
        public static String PASSWORD = "aklsjndcd128p";
        public static String IMEI = "alsdnc1092f12i";
        public static String WAREHOUSE_NUMBER = "waksjndkj12id2w";
        public static String STORAGE_TYPE = "askdj12u8fnd2s";
    }
}


package pk.com.ke.barcode_cs.api.models.opento;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Result {

    @SerializedName("__metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("Whsenumber")
    @Expose
    private String warehouseNumber;
    @SerializedName("TransOrd")
    @Expose
    private String transferOrder;
    @SerializedName("ToItem")
    @Expose
    private String toItem;
    @SerializedName("Plant")
    @Expose
    private String plant;
    @SerializedName("Batch")
    @Expose
    private String batch;
    @SerializedName("StockCat")
    @Expose
    private String stockCat;
    @SerializedName("SpecStock")
    @Expose
    private String specStockIndicator;
    @SerializedName("SpStckNo")
    @Expose
    private String specStockNumber;
    @SerializedName("BaseUom")
    @Expose
    private String unit;
    @SerializedName("CurrNo")
    @Expose
    private String currNo;
    @SerializedName("ConfirmInd")
    @Expose
    private String confirmationIndicator;
    @SerializedName("ConfDate")
    @Expose
    private String confirmationDate;
    @SerializedName("ConfTime")
    @Expose
    private String confirmationTime;
    @SerializedName("ToUser")
    @Expose
    private String toUser;
    @SerializedName("SourceType")
    @Expose
    private String sourceType;
    @SerializedName("Srcstorsec")
    @Expose
    private String sourceStorageSection;
    @SerializedName("SourceBin")
    @Expose
    private String sourceBin;
    @SerializedName("Srctgtqtyb")
    @Expose
    private String sourceTargetQuantity;
    @SerializedName("SrcAQtyb")
    @Expose
    private String sourceActualQuantity;
    @SerializedName("SrcBDifb")
    @Expose
    private String sourceStorageDifference;
    @SerializedName("Quant")
    @Expose
    private String quant;
    @SerializedName("DestType")
    @Expose
    private String destType;
    @SerializedName("DestSect")
    @Expose
    private String destStorageSection;
    @SerializedName("DestBin")
    @Expose
    private String destBin;
    @SerializedName("DestTargb")
    @Expose
    private String destTargetQuantity;
    @SerializedName("DestActb")
    @Expose
    private String destActualQuantity;
    @SerializedName("DestDifb")
    @Expose
    private String destDifferenceQuantity;
    @SerializedName("DestQuant")
    @Expose
    private String destQuant;
    @SerializedName("DifType")
    @Expose
    private String diffStorageType;
    @SerializedName("DifBin")
    @Expose
    private String difBin;
    @SerializedName("DifQuant")
    @Expose
    private String difQuant;
    @SerializedName("DifQtyb")
    @Expose
    private String difQuantityPosting;
    @SerializedName("StgeLoc")
    @Expose
    private String storageLocation;
    @SerializedName("WAREHOUSENUMBER")
    @Expose
    private String wAREHOUSENUMBER;
    @SerializedName("LGTYP")
    @Expose
    private String storageType;
    @SerializedName("IM_USERNAME")
    @Expose
    private String username;
    @SerializedName("EX_CODE")
    @Expose
    private String responseCode;
    @SerializedName("EX_MESSAGE")
    @Expose
    private String responseMessage;
    @SerializedName("MATNR")
    @Expose
    private String materialNumber;
    @SerializedName("MAKTX")
    @Expose
    private String materialDescription;
    @SerializedName("EBELN")
    @Expose
    private String poNumber;
    @SerializedName("TO_ORDER_TYPE")
    @Expose
    private String transferOrderType;



    @SerializedName("IS_SERIALIZED")
    @Expose
    private String isSerialized;
    public String getIsSerialized() {
        return isSerialized;
    }

    public void setIsSerialized(String isSerialized) {
        this.isSerialized = isSerialized;
    }



    public String getMaterialNumber() {
        return materialNumber;
    }

    public void setMaterialNumber(String materialNumber) {
        this.materialNumber = materialNumber;
    }

    public String getMaterialDescription() {
        return materialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getTransferOrderType() {
        return transferOrderType;
    }

    public void setTransferOrderType(String transferOrderType) {
        this.transferOrderType = transferOrderType;
    }




//    public Metadata getMetadata() {
//        return metadata;
//    }
//
//    public void setMetadata(Metadata metadata) {
//        this.metadata = metadata;
//    }

    public String getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseNumber(String warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public String getTransferOrder() {
        return transferOrder;
    }

    public void setTransferOrder(String transferOrder) {
        this.transferOrder = transferOrder;
    }

    public String getToItem() {
        return toItem;
    }

    public void setToItem(String toItem) {
        this.toItem = toItem;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getStockCat() {
        return stockCat;
    }

    public void setStockCat(String stockCat) {
        this.stockCat = stockCat;
    }

    public String getSpecStockIndicator() {
        return specStockIndicator;
    }

    public void setSpecStockIndicator(String specStockIndicator) {
        this.specStockIndicator = specStockIndicator;
    }

    public String getSpecStockNumber() {
        return specStockNumber;
    }

    public void setSpecStockNumber(String specStockNumber) {
        this.specStockNumber = specStockNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCurrNo() {
        return currNo;
    }

    public void setCurrNo(String currNo) {
        this.currNo = currNo;
    }

    public String getConfirmationIndicator() {
        return confirmationIndicator;
    }

    public void setConfirmationIndicator(String confirmationIndicator) {
        this.confirmationIndicator = confirmationIndicator;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(String confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceStorageSection() {
        return sourceStorageSection;
    }

    public void setSourceStorageSection(String sourceStorageSection) {
        this.sourceStorageSection = sourceStorageSection;
    }

    public String getSourceBin() {
        return sourceBin;
    }

    public void setSourceBin(String sourceBin) {
        this.sourceBin = sourceBin;
    }

    public String getSourceTargetQuantity() {
        return sourceTargetQuantity;
    }

    public void setSourceTargetQuantity(String sourceTargetQuantity) {
        this.sourceTargetQuantity = sourceTargetQuantity;
    }

    public String getSourceActualQuantity() {
        return sourceActualQuantity;
    }

    public void setSourceActualQuantity(String sourceActualQuantity) {
        this.sourceActualQuantity = sourceActualQuantity;
    }

    public String getSourceStorageDifference() {
        return sourceStorageDifference;
    }

    public void setSourceStorageDifference(String sourceStorageDifference) {
        this.sourceStorageDifference = sourceStorageDifference;
    }

    public String getQuant() {
        return quant;
    }

    public void setQuant(String quant) {
        this.quant = quant;
    }

    public String getDestType() {
        return destType;
    }

    public void setDestType(String destType) {
        this.destType = destType;
    }

    public String getDestStorageSection() {
        return destStorageSection;
    }

    public void setDestStorageSection(String destStorageSection) {
        this.destStorageSection = destStorageSection;
    }

    public String getDestBin() {
        return destBin;
    }

    public void setDestBin(String destBin) {
        this.destBin = destBin;
    }

    public String getDestTargetQuantity() {
        return destTargetQuantity;
    }

    public void setDestTargetQuantity(String destTargetQuantity) {
        this.destTargetQuantity = destTargetQuantity;
    }

    public String getDestActualQuantity() {
        return destActualQuantity;
    }

    public void setDestActualQuantity(String destActualQuantity) {
        this.destActualQuantity = destActualQuantity;
    }

    public String getDestDifferenceQuantity() {
        return destDifferenceQuantity;
    }

    public void setDestDifferenceQuantity(String destDifferenceQuantity) {
        this.destDifferenceQuantity = destDifferenceQuantity;
    }

    public String getDestQuant() {
        return destQuant;
    }

    public void setDestQuant(String destQuant) {
        this.destQuant = destQuant;
    }

    public String getDiffStorageType() {
        return diffStorageType;
    }

    public void setDiffStorageType(String diffStorageType) {
        this.diffStorageType = diffStorageType;
    }

    public String getDifBin() {
        return difBin;
    }

    public void setDifBin(String difBin) {
        this.difBin = difBin;
    }

    public String getDifQuant() {
        return difQuant;
    }

    public void setDifQuant(String difQuant) {
        this.difQuant = difQuant;
    }

    public String getDifQuantityPosting() {
        return difQuantityPosting;
    }

    public void setDifQuantityPosting(String difQuantityPosting) {
        this.difQuantityPosting = difQuantityPosting;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getWAREHOUSENUMBER() {
        return wAREHOUSENUMBER;
    }

    public void setWAREHOUSENUMBER(String wAREHOUSENUMBER) {
        this.wAREHOUSENUMBER = wAREHOUSENUMBER;
    }

    public String getwAREHOUSENUMBER() {
        return wAREHOUSENUMBER;
    }

    public void setwAREHOUSENUMBER(String wAREHOUSENUMBER) {
        this.wAREHOUSENUMBER = wAREHOUSENUMBER;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}

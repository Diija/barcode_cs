package pk.com.ke.barcode_cs.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.HomeActivity;
import pk.com.ke.barcode_cs.activities.base.BaseActivity;
import pk.com.ke.barcode_cs.adapter.BaseSectionAdapter;
import pk.com.ke.barcode_cs.adapter.RecyclerAdapter;
import pk.com.ke.barcode_cs.api.ApiInterface;
import pk.com.ke.barcode_cs.api.RetrofitInstance;
import pk.com.ke.barcode_cs.api.UrlProvider;
import pk.com.ke.barcode_cs.api.models.opento.Result;
import pk.com.ke.barcode_cs.api.models.toassignment.TOAssignmentModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllTO extends Fragment implements HomeActivity.FragmentDataProvider , BaseSectionAdapter.OnItemClickListener {

    RecyclerAdapter mSectionedRecyclerAdapter;
    private RecyclerView recyclerView;
    private OnFragmentInteractionListener mListener;

    public AllTO() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all_to, container, false);
//        return inflater.inflate(R.layout.fragment_assigned_to_me, container, false);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        providedData(null);
        recyclerView = getActivity().findViewById(R.id.recyclerView);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void providedData(List<Result> results) {

        if (isAdded()) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            Comparator<Result> movieComparator = (o1, o2) -> o1.getToItem().compareTo(o2.getToItem());
            Collections.sort(results, movieComparator);
            mSectionedRecyclerAdapter = new RecyclerAdapter(getActivity(), results);

            mSectionedRecyclerAdapter.setOnItemClickListener(this);

            recyclerView.setAdapter(mSectionedRecyclerAdapter);
        }
    }


    public void onItemClicked(Result movie, int position) {

    }

    @Override
    public void onSubheaderClicked(int position) {
        Toast.makeText(getActivity(), "SubHeader Clicked:" + position, Toast.LENGTH_SHORT).show();

        if (mSectionedRecyclerAdapter.isSectionExpanded(mSectionedRecyclerAdapter.getSectionIndex(position))) {
            mSectionedRecyclerAdapter.collapseSection(mSectionedRecyclerAdapter.getSectionIndex(position));
        } else {
            mSectionedRecyclerAdapter.expandSection(mSectionedRecyclerAdapter.getSectionIndex(position));
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}

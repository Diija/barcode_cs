
package pk.com.ke.barcode_cs.api.models.toassignment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class D {

    @SerializedName("__metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("Lgnum")
    @Expose
    private String warehouseNumber;
    @SerializedName("PickBy")
    @Expose
    private String pickBy;
    @SerializedName("Tanum")
    @Expose
    private String transferOrder;
    @SerializedName("Tapos")
    @Expose
    private String toItem;
    @SerializedName("ExCode")
    @Expose
    private String responseCode;
    @SerializedName("ExMessage")
    @Expose
    private String responseMessage;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseNumber(String warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public String getPickBy() {
        return pickBy;
    }

    public void setPickBy(String pickBy) {
        this.pickBy = pickBy;
    }

    public String getTransferOrder() {
        return transferOrder;
    }

    public void setTransferOrder(String transferOrder) {
        this.transferOrder = transferOrder;
    }

    public String getToItem() {
        return toItem;
    }

    public void setToItem(String toItem) {
        this.toItem = toItem;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

}

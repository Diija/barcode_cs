package pk.com.ke.barcode_cs.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.EdgeDetail;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.charts.SeriesLabel;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.utils.EaseOutElasticInterpolator;

public class DecoTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deco_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


//some thing
        //decoview config
        DecoView arcView = (DecoView)findViewById(R.id.dynamicArcView);
        TextView txtview = findViewById(R.id.txtView_DecoLabel);
// Create background track

        SeriesItem seriesBackground = new SeriesItem.Builder(Color.parseColor("#c8d6e5"))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(44f)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_DONUT)
                .setShadowColor(Color.parseColor("#576574"))
                .setShadowSize(46f)
                .build();
        arcView.addSeries(seriesBackground);



        //Create data series track
        SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#feca57"))
                .setRange(0, 100, 0)
                .setInitialVisibility(false)
                .setLineWidth(40f)
                .addEdgeDetail(new EdgeDetail(EdgeDetail.EdgeType.EDGE_OUTER, Color.parseColor("#ff9f43"), 0.4f))
                .setChartStyle(SeriesItem.ChartStyle.STYLE_DONUT)
//                .setSeriesLabel(new SeriesLabel.Builder("Percent %.0f%%")
////                        .setColorBack(Color.argb(218, 0, 0, 0))
////                        .setColorText(Color.argb(255, 255, 255, 255))
////                        .build())
                .setShadowColor(Color.argb(100,200,200,200))
                .build();


        int series1Index = arcView.addSeries(seriesItem1);
        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .setDisplayText("Display Text")

                .build());

        String format = "%.0f%5%";
        String format2 = "";

        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                if (format.contains("%%")) {
                    float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                    txtview.setText(String.format(format, percentFilled * 100f));
                }else if (format2.contains("")){
                    txtview.setText(String.format("%.0f",currentPosition)+"/"+seriesItem1.getMaxValue());
                }
                else {
                    txtview.setText(String.format(format, currentPosition));
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });


        arcView.isClickable();
        arcView.setOnClickListener(v -> {
            arcView.animate()
                    .setInterpolator(new EaseOutElasticInterpolator(4.5f))
                    .setDuration(1000)
                    .scaleX(0.95f)
                    .scaleY(0.95f);
            arcView.addEvent(new DecoEvent.Builder(20).setIndex(series1Index).build());
////            arcView.setScaleX(0.8f);
//            arcView.animate()
//                    .setInterpolator(new EaseOutElasticInterpolator(2.5f))
//                    .setStartDelay(5000)
//                    .setDuration(1000)
//                    .scaleX(1.0f)
//                    .scaleY(1.0f);
//            arcView.animate()
//                    .setInterpolator(new EaseOutElasticInterpolator(2.5f))
//                    .setDuration(1000)
//                    .scaleX(1.0f)
//                    .scaleY(1.0f);
        });


//        arcView.addEvent(new DecoEvent.Builder(25).setIndex(series1Index).setDelay(4000).build());
//        arcView.addEvent(new DecoEvent.Builder(100).setIndex(series1Index).setDelay(8000).build());
//        arcView.addEvent(new DecoEvent.Builder(10).setIndex(series1Index).setDelay(12000).build());
//        arcView.addEvent(new DecoEvent.Builder(35).setIndex(series1Index).setDelay(500).build());
//        arcView.addEvent(new DecoEvent.Builder(18).setIndex(series1Index).setDelay(2000).build());

        //        arcView.addEvent(new DecoEvent.Builder(14).setIndex(series1Index).setDe]lay(2000).build());
//        arcView.addEvent(new DecoEvent.Builder(15).setIndex(series1Index).setDelay(3000).build());
//        arcView.addEvent(new DecoEvent.Builder(70).setIndex(series1Index).setDelay(4000).build());
//        arcView.addEvent(new DecoEvent.Builder(80).setIndex(series1Index).setDelay(5000).build());
//        arcView.addEvent(new DecoEvent.Builder(78).setIndex(series1Index).setDelay(6000).build());


    }
}

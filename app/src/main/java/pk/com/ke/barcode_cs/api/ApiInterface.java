package pk.com.ke.barcode_cs.api;

import okhttp3.ResponseBody;
import pk.com.ke.barcode_cs.activities.Constants;
import pk.com.ke.barcode_cs.api.models.login.LoginModel;
import pk.com.ke.barcode_cs.api.models.opento.OpenTOModel;
import pk.com.ke.barcode_cs.api.models.pick_item.PickItem;
import pk.com.ke.barcode_cs.api.models.toassignment.TOAssignmentModel;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET
    Call<LoginModel> Login (@Url String URL);

    @GET
    Call<OpenTOModel> openToService (@Url String formedUrl);

    @GET
    Call<TOAssignmentModel> toAssignmentService (@Url String formedUrl);

    @GET
    Call<PickItem> pickItemService (@Url String formedUrl);

}

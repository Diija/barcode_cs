//package pk.com.ke.barcode_cs.activities;
//
//import android.os.Bundle;
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageInfo;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.Menu;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import pk.com.ke.barcode_cs.R;
//import pk.com.ke.barcode_cs.activities.Constants;
//
//public class MainActivity extends Activity {
//    private EditText mResultText;
//    private EditText mSymbologyInput;
//    private EditText mValue1Input;
//    private EditText mModeInput;
//    private EditText mValue2Input;
//    private TextView mStatusText;
//    private TextView mSuccessFailText;
//
//    private String mCurrentStatus;
//    private boolean mIsRegisterReceiver;
//
//    private static final String STATUS_CLOSE = "STATUS_CLOSE";
//    private static final String STATUS_OPEN = "STATUS_OPEN";
//    private static final String STATUS_TRIGGER_ON = "STATUS_TRIGGER_ON";
//
//    private static final int SEQ_BARCODE_OPEN = 100;
//    private static final int SEQ_BARCODE_CLOSE = 200;
//    private static final int SEQ_BARCODE_GET_STATUS = 300;
//    private static final int SEQ_BARCODE_SET_TRIGGER_ON = 400;
//    private static final int SEQ_BARCODE_SET_TRIGGER_OFF = 500;
//
//    TextView timeTaken;
//    long startNanos, endNanos;
//    boolean isHBPressed = false;
//
//    Integer[] KEYCODES_IntArray = new Integer[]{280, 281};
//    List<Integer> KEYCODES = Arrays.asList(KEYCODES_IntArray);
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        initialize();
//
//        registerReceiver();
//    }
//
//    @Override
//    protected void onResume() {
//        registerReceiver();
//        super.onResume();
////        resetTimers();
//
//    }
//
//    @Override
//    protected void onPause() {
//        unregisterReceiver();
//        super.onPause();
//    }
//
//    private void initialize() {
//        setTitle(getString(R.string.app_name) + " v" + getVersion());
//        initLayout();
//
//        mIsRegisterReceiver = false;
//
//        Button btn = (Button) findViewById(R.id.btn1);
//        btn.setOnClickListener(mBtnClickListener);
//        btn = (Button) findViewById(R.id.btn2);
//        btn.setOnClickListener(mBtnClickListener);
//        btn = (Button) findViewById(R.id.btn3);
//        btn.setOnClickListener(mBtnClickListener);
//        btn = (Button) findViewById(R.id.btn4);
//        btn.setOnClickListener(mBtnClickListener);
//        btn = (Button) findViewById(R.id.btn5);
//        btn.setOnClickListener(mBtnClickListener);
//
//        timeTaken = (TextView) findViewById(R.id.time);
//    }
//
//    private String getVersion() {
//        String version = "";
//        try {
//            PackageInfo pInfo = getPackageManager().getPackageInfo(
//                    getPackageName(), 0);
//            version = pInfo.versionName;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return version;
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
////        Toast.makeText(this, "KeyCode:" + keyCode, Toast.LENGTH_SHORT).show();
//
//        if (KEYCODES.contains(keyCode)) {
//            if (!isHBPressed) {
//                isHBPressed = true;
//                startTime();
//            }
//        }
//
//        return super.onKeyDown(keyCode, event);
//    }
//
//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//
//        Toast.makeText(this, "KeyCodeUp:" + keyCode, Toast.LENGTH_SHORT).show();
//
//        if (KEYCODES.contains(keyCode)) {
//            if (isHBPressed) {
//                isHBPressed = false;
//            }
//        }
//
//        return super.onKeyUp(keyCode, event);
//    }
//
//    private boolean mIsOpened = false;
//
//    Button.OnClickListener mBtnClickListener = new Button.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent intent = new Intent();
//            switch (v.getId()) {
//                case R.id.btn1:
//                    mCount = 0;
//                    intent.setAction(Constants.ACTION_BARCODE_OPEN);
//                    if (mIsOpened)
//                        intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//                    intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_OPEN);
//                    sendBroadcast(intent);
//                    mIsOpened = true;
//                    setResultText("BARCODE_OPEN");
//                    break;
//                case R.id.btn2:
//                    intent.setAction(Constants.ACTION_BARCODE_CLOSE);
//                    intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//                    intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_CLOSE);
//                    sendBroadcast(intent);
//                    mIsOpened = false;
//                    setResultText("BARCODE_CLOSE");
//                    break;
//
//                case R.id.btn3:
//                    intent.setAction(Constants.ACTION_BARCODE_SET_TRIGGER);
//                    intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//                    intent.putExtra(Constants.EXTRA_INT_DATA2, 1);
//                    intent.putExtra(Constants.EXTRA_INT_DATA3,
//                            SEQ_BARCODE_SET_TRIGGER_ON);
//                    sendBroadcast(intent);
//                    setResultText("SET_TRIGGER : ON");
//
//                    startTime();
//
//                    break;
//
//                case R.id.btn4:
//                    intent.setAction(Constants.ACTION_BARCODE_SET_TRIGGER);
//                    intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//                    intent.putExtra(Constants.EXTRA_INT_DATA2, 0);
//                    intent.putExtra(Constants.EXTRA_INT_DATA3,
//                            SEQ_BARCODE_SET_TRIGGER_OFF);
//                    sendBroadcast(intent);
//                    setResultText("SET_TRIGGER : OFF");
//                    break;
//
//                case R.id.btn5:
//                    intent.setAction(Constants.ACTION_BARCODE_GET_STATUS);
//                    intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//                    intent.putExtra(Constants.EXTRA_INT_DATA3,
//                            SEQ_BARCODE_GET_STATUS);
//                    sendBroadcast(intent);
//                    setResultText("BARCODE_GET_STATUS");
//                    break;
//            }
//        }
//    };
//
//    private void registerReceiver() {
//        if (mIsRegisterReceiver)
//            return;
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_DECODING_DATA);
//        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS);
//        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_REQUEST_FAILED);
//        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_GET_STATUS);
//
//        registerReceiver(mReceiver, filter);
//        mIsRegisterReceiver = true;
//    }
//
//    private void unregisterReceiver() {
//        if (!mIsRegisterReceiver)
//            return;
//        unregisterReceiver(mReceiver);
//        mIsRegisterReceiver = false;
//    }
//
//
//    void startTime() {
//        resetTimers();
//        startNanos = System.currentTimeMillis();
//        Log.e("BC", "Start:" + startNanos);
//    }
//
//    void endTime() {
//        endNanos = System.currentTimeMillis();
//        Log.e("BC", "End:" + startNanos);
//    }
//
//    void resetTimers() {
////        startNanos = 0;
////        endNanos = 0;
//    }
//
//    long getTimeDiff() {
//        long diffTime = /*TimeUnit.MILLISECONDS.toSeconds(*/endNanos - startNanos/*)*/;
//
//        Log.e("BC", "diffTime:" + diffTime + " m.sec");
//        return diffTime;
//    }
//
//    private int mBarcodeHandle;
//    private int mCount = 0;
//
//    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            int seq = intent.getIntExtra(Constants.EXTRA_INT_DATA3, 0);
//
//            if (action.equals(Constants.ACTION_BARCODE_CALLBACK_DECODING_DATA)) {
//
//                endTime();
//                timeTaken.setText("Time Taken:" + getTimeDiff());
//
//                int handle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
//                byte[] data = intent
//                        .getByteArrayExtra(Constants.EXTRA_BARCODE_DECODING_DATA);
//                String result = "[BarcodeDecodingData handle : " + handle
//                        + " / count : " + mCount + " / seq : " + seq + "]\n";
//                if (data != null)
//                    result += "[Data] : " + new String(data);
//                setResultText(result);
//                mCount++;
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS)) {
//                mBarcodeHandle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
//                setSuccessFailText("Success : " + seq);
//                if (seq == SEQ_BARCODE_OPEN) {
//                    mCurrentStatus = STATUS_OPEN;
//                } else if (seq == SEQ_BARCODE_CLOSE) {
//                    mCurrentStatus = STATUS_CLOSE;
//                }
//
//                refreshCurrentStatus();
//
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_FAILED)) {
//                int result = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
//                if (result == Constants.ERROR_BARCODE_DECODING_TIMEOUT) {
//                    setSuccessFailText("Failed result : " + "Decode Timeout"
//                            + " / seq : " + seq);
//                } else if (result == Constants.ERROR_NOT_SUPPORTED) {
//                    setSuccessFailText("Failed result : " + "Not Supoorted"
//                            + " / seq : " + seq);
//                } else if (result == Constants.ERROR_BARCODE_ERROR_USE_TIMEOUT) {
//                    mCurrentStatus = STATUS_CLOSE;
//                    setSuccessFailText("Failed result : " + "Use Timeout"
//                            + " / seq : " + seq);
//                } else
//                    setSuccessFailText("Failed result : " + result
//                            + " / seq : " + seq);
//                refreshCurrentStatus();
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_GET_STATUS)) {
//                int status = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
//
//                switch (status) {
//                    case 0:
//                        mCurrentStatus = STATUS_CLOSE;
//                        break;
//                    case 1:
//                        mCurrentStatus = STATUS_OPEN;
//                        break;
//                    case 2:
//                        mCurrentStatus = STATUS_TRIGGER_ON;
//                        break;
//                }
//                setResultText(mCurrentStatus);
//            }
//        }
//    };
//
//    private void initLayout() {
//        ((TextView) findViewById(R.id.resultTitle)).setText("Result");
//        ((Button) findViewById(R.id.btn1)).setText("Open");
//        ((Button) findViewById(R.id.btn2)).setText("Close");
//
//        mResultText = (EditText) findViewById(R.id.resultTxt);
//        mStatusText = (TextView) findViewById(R.id.statusTxt);
//        mSuccessFailText = (TextView) findViewById(R.id.successTxt);
//    }
//
//    private void setResultText(String text) {
//        mResultText.setText(text);
//    }
//
//    private void refreshCurrentStatus() {
//        mStatusText.setText("Status : " + mCurrentStatus);
//    }
//
//    private void setSuccessFailText(String text) {
//        mSuccessFailText.setText(text);
//    }
//
//    private void showToast(String text) {
//        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
////        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    protected void onUserLeaveHint() {
//        onBackPressed();
//        super.onUserLeaveHint();
//    }
//
//    private void destroyEvent() {
//
//        if (mIsOpened) {
//            Intent intent = new Intent();
//            intent.setAction(Constants.ACTION_BARCODE_CLOSE);
//            intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
//            intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_CLOSE);
//            sendBroadcast(intent);
//        }
//        unregisterReceiver();
//    }
//
//    @Override
//    protected void onDestroy() {
//        destroyEvent();
//        super.onDestroy();
//    }
//}

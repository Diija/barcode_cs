package pk.com.ke.barcode_cs.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.api.models.opento.Result;

public class RecyclerAdapter extends BaseSectionAdapter implements Filterable {

    private List<Result> itemList;
    Context ctx;

    public RecyclerAdapter(Context ctx, List<Result> itemList) {
        super(itemList);
        this.ctx = ctx;
        this.itemList = itemList;
    }

    @Override
    public boolean onPlaceSubheaderBetweenItems(int position) {
        final Result transferOrder = transferOrderList.get(position);
        final Result nexttransferOrder = transferOrderList.get(position + 1);

        return !transferOrder.getTransferOrder().equals(nexttransferOrder.getTransferOrder());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindItemViewHolder(final transferOrderViewHolder holder, final int position) {
        final Result transferOrder = transferOrderList.get(position);
        if (transferOrder.getToUser().trim().equals("")) {
            holder.viewAssignmentIndicator.setBackgroundColor(ctx.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.viewAssignmentIndicator.setBackgroundColor(ctx.getResources().getColor(R.color.fontcolor));
        }

        if (transferOrder.getTransferOrderType().equals("ISS")) {
            holder.textBadge.setText("Issuance");
            holder.textBadge.setBackgroundTintList(ctx.getResources().getColorStateList(R.color.badgecolor));
        } else if (transferOrder.getTransferOrderType().equals("RECV")) {
            holder.textBadge.setText("Receiving");
            holder.textBadge.setBackgroundTintList(ctx.getResources().getColorStateList(R.color.Tint2));

        } else /*if(transferOrder.getTransferOrderType().equals("B2B"))*/ {
            holder.textBadge.setText("Bin 2 Bin");
            holder.textBadge.setBackgroundTintList(ctx.getResources().getColorStateList(R.color.Tint1));
        }

        holder.texDestination.setText(transferOrder.getDestBin());
        holder.textSource.setText(transferOrder.getSourceBin());
        holder.textMaterialCode.setText(transferOrder.getMaterialNumber().replaceFirst("^0+(?!$)", ""));
        holder.textTransferOrder.setText(transferOrder.getToItem() + "/" + transferOrder.getTransferOrder());
//        holder.textQuantity.setText(transferOrder.getDestActualQuantity().split("\\.")[0] + "/" + transferOrder.getDestTargetQuantity().split("\\.")[0]);
        holder.textQuantity.setText(transferOrder.getDestActualQuantity() + "/" + transferOrder.getDestTargetQuantity());
        holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClicked(transferOrder, position));
    }

    @Override
    public void onBindSubheaderViewHolder(SubheaderHolder subheaderHolder, int nextItemPosition) {
        super.onBindSubheaderViewHolder(subheaderHolder, nextItemPosition);
        final Result nexttransferOrder = transferOrderList.get(nextItemPosition);
        subheaderHolder.mSubheaderText.setText(nexttransferOrder.getTransferOrder());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<Result> filteredList = new ArrayList<>();

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = itemList;
                } else {
                    for (Result r : itemList) {
                        if (r.getToItem().contains(charString)
                                || r.getBatch().contains(charString)) {
                            //@TODO Need to add searchable columns here
                            filteredList.add(r);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemList = (ArrayList<Result>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}

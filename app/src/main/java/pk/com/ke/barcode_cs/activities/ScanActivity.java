package pk.com.ke.barcode_cs.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.eminayar.panter.DialogType;
import com.eminayar.panter.PanterDialog;
import com.eminayar.panter.enums.Animation;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.EdgeDetail;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;

import org.parceler.Parcels;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pk.com.ke.barcode_cs.Config;
import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.base.BaseActivity;
import pk.com.ke.barcode_cs.api.ApiInterface;
import pk.com.ke.barcode_cs.api.RetrofitInstance;
import pk.com.ke.barcode_cs.api.UrlProvider;
import pk.com.ke.barcode_cs.api.models.opento.Result;
import pk.com.ke.barcode_cs.api.models.pick_item.PickItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.DialogInterface.BUTTON_POSITIVE;
import static pk.com.ke.barcode_cs.activities.ScanActivity.Constants.KEYCODES;

public class ScanActivity extends BaseActivity {

    private static final int SEQ_BTN_SCAN = 111;
    private static final int SEQ_BTN_START_SERIAL = 222;
    private static final int SEQ_BTN_END_SERIAL = 333;
    private static final int SEQ_BTN_SOURCE_BIN = 444;
    private static final int SEQ_BTN_DEST_BIN = 555;
    private static final int SEQ_BTN_POST = 666;

    private static final String QUANTITY_DIALOG = "quantity";

    private int btn_id = SEQ_BTN_SCAN;
    private int tmp_btn_id = SEQ_BTN_SCAN;
    private boolean IS_SERIALIZED = false;

    Result transferOrder;
    boolean firstScanned = false;

    float targetQty, initialPickedQty, currentPickedQty;
    int series1Index;

    @BindView(R.id.ll_Series_1)
    LinearLayout ll_Series_1;
    @BindView(R.id.ll_Series_2)
    LinearLayout ll_Series_2;
    @BindView(R.id.ll_Series_3)
    LinearLayout ll_Series_3;
    @BindView(R.id.ll_Series_4)
    LinearLayout ll_Series_4;
    @BindView(R.id.ll_Series_5)
    LinearLayout ll_Series_5;

    @BindView(R.id.addBtn)
    ImageButton addBtn;

    @BindView(R.id.txtScanBadge)
    TextView txtScanBadge;
    @BindView(R.id.txtScanTransferOrder)
    TextView txtScanTransferOrder;
    @BindView(R.id.txtScanMaterialCode)
    TextView txtScanMaterialCode;
    @BindView(R.id.txtScanDestination)
    TextView txtScanDestination;
    @BindView(R.id.txtScanMaterialText)
    TextView txtScanMaterialText;
    @BindView(R.id.txtScanSource)
    TextView txtScanSource;
    @BindView(R.id.dynamicArcView)
    DecoView arcView;
    @BindView(R.id.txtView_DecoLabel)
    TextView txtview;

    @BindView(R.id.editTxtSeriesStart)
    EditText edtSeriesStart;
    @BindView(R.id.editTxtSeriesEnd)
    EditText edtSeriesEnd;

    @BindView(R.id.editTxtSeriesStart2)
    EditText edtSeriesStart2;
    @BindView(R.id.editTxtSeriesEnd2)
    EditText edtSeriesEnd2;

    @BindView(R.id.editTxtSeriesStart3)
    EditText edtSeriesStart3;
    @BindView(R.id.editTxtSeriesEnd3)
    EditText edtSeriesEnd3;

    @BindView(R.id.editTxtSeriesStart4)
    EditText edtSeriesStart4;
    @BindView(R.id.editTxtSeriesEnd4)
    EditText edtSeriesEnd4;

    @BindView(R.id.editTxtSeriesStart5)
    EditText edtSeriesStart5;
    @BindView(R.id.editTxtSeriesEnd5)
    EditText edtSeriesEnd5;


    @BindView(R.id.cardViewSerialized)
    CardView cardViewSerialized;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnScan)
    Button btnScan;

    @BindView(R.id.btnStartSerial)
    Button btnStartSeries;
    @BindView(R.id.btnEndSerial)
    Button btnEndSeries;

    @BindView(R.id.btnStartSerial2)
    Button btnStartSeries2;
    @BindView(R.id.btnEndSerial2)
    Button btnEndSeries2;

    @BindView(R.id.btnStartSerial3)
    Button btnStartSeries3;
    @BindView(R.id.btnEndSerial3)
    Button btnEndSeries3;

    @BindView(R.id.btnStartSerial4)
    Button btnStartSeries4;
    @BindView(R.id.btnEndSerial4)
    Button btnEndSeries4;

    @BindView(R.id.btnStartSerial5)
    Button btnStartSeries5;
    @BindView(R.id.btnEndSerial5)
    Button btnEndSeries5;

    @BindView(R.id.btnPost)
    Button btnPost;


    @BindView(R.id.ll_Batch)
    CardView ll_Batch;
    @BindView(R.id.edtBatch)
    EditText edtBatch;


    private boolean isBatchable = false;

    String type = "";
    int SeriesCount = 1;
    int SeriesScanned = 0;
    private boolean isDestBinVerified = false;
    private boolean isSourceBinVerified = false;
    SeriesItem seriesItem1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        //Bind views
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.GONE);

        ll_Series_2.setVisibility(View.GONE);
        ll_Series_3.setVisibility(View.GONE);
        ll_Series_4.setVisibility(View.GONE);
        ll_Series_5.setVisibility(View.GONE);

        try {
            barcode_OnCreate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        transferOrder = Parcels.unwrap(getIntent().getParcelableExtra("transferOrder"));

        type = transferOrder.getTransferOrderType();
//        type = "BIN2BIN";

        buildInfoCard();

        targetQty = Float.parseFloat(transferOrder.getDestTargetQuantity());
        initialPickedQty = Float.parseFloat(transferOrder.getDestActualQuantity());
        currentPickedQty = initialPickedQty;
        if (initialPickedQty <= targetQty) {
            buildDecoview();
        }


        if (type.equals("ISS")) {
            btn_id = SEQ_BTN_SOURCE_BIN;
            isSourceBinVerified = false;
            isDestBinVerified = true;

            verifySourceBin();

        } else if (type.equals("RECV")) {
            btn_id = SEQ_BTN_SCAN;
            isSourceBinVerified = true;
            isDestBinVerified = false;

        } else {
            btn_id = SEQ_BTN_SOURCE_BIN;
            isSourceBinVerified = false;
            isDestBinVerified = false;

            // B2B, so first verify source bin, then scan material, then dest bin
            verifySourceBin();

        }


        if (TextUtils.isEmpty(transferOrder.getBatch())) {
            // Non Batch-able Item
            isBatchable = false;
            ll_Batch.setVisibility(View.GONE);
        } else {
            isBatchable = true;
            ll_Batch.setVisibility(View.VISIBLE);
            edtBatch.setText(transferOrder.getBatch());

            edtBatch.setFocusable(false);
            edtBatch.setEnabled(false);
            edtBatch.setCursorVisible(false);
            edtBatch.setKeyListener(null);
            edtBatch.setBackgroundColor(Color.TRANSPARENT);
        }

    }

    @OnClick(R.id.btnStartSerial)
    public void onClickStartSeries(View v) {
        SeriesScanned=0;
        doScan(SEQ_BTN_START_SERIAL);
    }

    @OnClick(R.id.btnStartSerial2)
    public void onClickStartSeries2(View v) {
        SeriesScanned=1;
        doScan(SEQ_BTN_START_SERIAL);
    }

    @OnClick(R.id.btnStartSerial3)
    public void onClickStartSeries3(View v) {
        SeriesScanned=2;
        doScan(SEQ_BTN_START_SERIAL);
    }

    @OnClick(R.id.btnStartSerial4)
    public void onClickStartSeries4(View v) {
        SeriesScanned=3;
        doScan(SEQ_BTN_START_SERIAL);
    }

    @OnClick(R.id.btnStartSerial5)
    public void onClickStartSeries5(View v) {
        SeriesScanned=4;
        doScan(SEQ_BTN_START_SERIAL);
    }


    @OnClick(R.id.btnEndSerial)
    public void onClickEndSeries(View v) {
        SeriesScanned=1;
        doScan(SEQ_BTN_END_SERIAL);
    }

    @OnClick(R.id.btnEndSerial2)
    public void onClickEndSeries2(View v) {
        SeriesScanned=2;
        doScan(SEQ_BTN_END_SERIAL);
    }

    @OnClick(R.id.btnEndSerial3)
    public void onClickEndSeries3(View v) {
        SeriesScanned=3;
        doScan(SEQ_BTN_END_SERIAL);
    }

    @OnClick(R.id.btnEndSerial4)
    public void onClickEndSeries4(View v) {
        SeriesScanned=4;
        doScan(SEQ_BTN_END_SERIAL);
    }

    @OnClick(R.id.btnEndSerial5)
    public void onClickEndSeries5(View v) {
        SeriesScanned=5;
        doScan(SEQ_BTN_END_SERIAL);
    }

    @OnClick(R.id.txtView_DecoLabel)
    public void onClickDecoCount(View v) {

        if (firstScanned) {
            new PanterDialog(this)
//                .setHeaderBackground(getResources().getColor(R.color.colorPrimaryDark))
                    .setTitle("Enter Quantity")
//                    .setHeaderLogo(R.mipmap.ic_launcher)
                    .setHeaderBackground(R.color.fontcolor)
                    .setDialogType(DialogType.INPUT)
                    .withAnimation(Animation.DEFAULT)
                    .isCancelable(true)
                    .input("Enter Quantity Here... (Remaining: " + Float.toString(targetQty - currentPickedQty) + ")",
                            text -> {
//                                Toast.makeText(ScanActivity.this, text, Toast.LENGTH_LONG).show();
                                if(text==""){
                                    currentPickedQty = targetQty;
                                    try {
                                        updateDecoView();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                else{
                                    try {

                                        float q = Float.parseFloat(text);

//                                    if ((currentPickedQty + q) <= targetQty) {
//                                        currentPickedQty += q;

                                        if (q <= 0) {
                                            toast("Quantity can't be 0 or less than 0", ToastType.INFO);

                                        } else if ((initialPickedQty + q) <= targetQty) {
                                            currentPickedQty = initialPickedQty + q;

                                            try {
                                                updateDecoView();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else {
                                            toast("Quantity can't be greater than " + (targetQty - initialPickedQty), ToastType.INFO);

                                        }


                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                        toast("Enter valid quantity : " + e.getMessage(), ToastType.ERROR);

                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        toast("Enter valid quantity : " + e.getMessage(), ToastType.ERROR);
                                    }
                                }

                            })

                    .show();

        } else {
            toast("Please scan material than you can enter quantity", ToastType.INFO);
        }

//        SimpleFormDialog.build()
//                .title("Scan Quantity")
//                .msg("Enter quantity")
//                .fields(
//                        Input.name("Quantity").required().hint("Quantity Picked").validatePatternAlphanumeric()
//                )
//                .show(this, QUANTITY_DIALOG);
    }

//    @Override
//    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
//        if (which == BUTTON_POSITIVE && QUANTITY_DIALOG.equals(dialogTag)) {
//            String quantity = extras.getString("Quantity");
//
//            int q = Integer.parseInt(quantity);
//
//            if (q <= targetQty) {
//                currentPickedQty = q;
//
//                try {
//                    arcView.addEvent(new DecoEvent.Builder(currentPickedQty).setIndex(series1Index).setDelay(500).build());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                return true;
//
//            } else if (q <= 0) {
//                toast("Quantity can't be less than 0", ToastType.INFO);
//
//            } else {
//                toast("Quantity can't be greater than ", ToastType.INFO);
//
//            }
//        }
//
//        return false;
//    }

    @Override
    public void onBackPressed() {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }

    private void verifyDestBin() {
        isDestBinVerified = false;

        //        showDialog("Scan Destination Bin", "You need to scan and verify the destination bin", Dialog_Type.PROMPT, "Scan", () -> {
//
//            doScan(SEQ_BTN_DEST_BIN);
//        });

        dialog = showScanDialog("Scan Destination Bin", "You need to scan and verify the destination bin(" + transferOrder.getDestBin() + ")", "Scan", view -> doScan(SEQ_BTN_DEST_BIN));
    }

    Dialog dialog;

    Dialog showScanDialog(String title, String message, String positiveText, View.OnClickListener onClickListener) {
        Dialog d = new Dialog(ScanActivity.this);
        d.setContentView(R.layout.dialog_scan);

        d.setTitle(title);
        TextView tv = d.findViewById(R.id.tv_Message);


        tv.setText(message);

        Button scan = d.findViewById(R.id.btn_Scan);
        scan.setText(positiveText);

        scan.setOnClickListener(onClickListener);

//        d.setCancelable(false);
        d.setCanceledOnTouchOutside(false);

        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });
        d.show();

        return d;
    }

    private void verifySourceBin() {
        isSourceBinVerified = false;
//        showDialog("Scan Source Bin", "You need to scan and verify the source bin", Dialog_Type.PROMPT, "Scan", () -> { doScan(SEQ_BTN_SOURCE_BIN);});

        dialog = showScanDialog("Scan Source Bin", "You need to scan and verify the source bin(" + transferOrder.getSourceBin() + ")", "Scan", view -> doScan(SEQ_BTN_SOURCE_BIN));
    }

    @SuppressLint("SetTextI18n")
    public void buildInfoCard() {
        txtScanDestination.setText(transferOrder.getSourceBin());
        txtScanMaterialCode.setText(transferOrder.getMaterialNumber().replaceFirst("^0+(?!$)", ""));
        txtScanMaterialText.setText(transferOrder.getMaterialDescription());
        txtScanSource.setText(transferOrder.getSourceBin());
        txtScanTransferOrder.setText(transferOrder.getTransferOrder() + "/" + transferOrder.getToItem());
        txtScanDestination.setText(transferOrder.getDestBin());


        if (type.equals("ISS")) {
            txtScanBadge.setText("Issuance");
            txtScanBadge.setBackgroundTintList(getResources().getColorStateList(R.color.badgecolor));

            serializeHandling();

        } else if (type.equals("RECV")) {
            txtScanBadge.setText("Receiving");
            txtScanBadge.setBackgroundTintList(getResources().getColorStateList(R.color.Tint2));

            serializeHandling();

        } else /*if(transferOrder.getTransferOrderType().equals("B2B"))*/ {
            txtScanBadge.setText("Bin 2 Bin");
            txtScanBadge.setBackgroundTintList(getResources().getColorStateList(R.color.Tint1));

            cardViewSerialized.setVisibility(View.GONE);
            arcView.setVisibility(View.GONE);
            ll_Series_1.setVisibility(View.GONE);

            btnScan.setVisibility(View.VISIBLE);
        }
    }

    void serializeHandling() {
        //serialized item
        if (!transferOrder.getIsSerialized().equals("X")) {
            cardViewSerialized.setVisibility(View.GONE);
            arcView.setVisibility(View.GONE);
            ll_Series_1.setVisibility(View.GONE);

            btnScan.setVisibility(View.VISIBLE);

        } else {

            cardViewSerialized.setVisibility(View.VISIBLE);
            arcView.setVisibility(View.VISIBLE);
            ll_Series_1.setVisibility(View.VISIBLE);
            IS_SERIALIZED = true;
            btnScan.setVisibility(View.VISIBLE);
        }
    }

    public void buildDecoview() {
//        targetQty = Integer.parseInt(transferOrder.getDestTargetQuantity().split("\\.")[0]);
//        initialPickedQty = Integer.parseInt(transferOrder.getDestActualQuantity().split("\\.")[0]);
//        currentPickedQty = initialPickedQty;

        targetQty = Float.parseFloat(transferOrder.getDestTargetQuantity());
        initialPickedQty = Float.parseFloat(transferOrder.getDestActualQuantity());
        currentPickedQty = initialPickedQty;

        txtview.setText(String.format("%.3f", initialPickedQty) + "/" + String.format("%.3f", targetQty));

        SeriesItem seriesBackground = new SeriesItem.Builder(Color.parseColor("#2c2c54"))
                .setRange(0, targetQty, targetQty)
                .setInitialVisibility(false)
                .setLineWidth(24f)
                .setChartStyle(SeriesItem.ChartStyle.STYLE_DONUT)
                .setShadowColor(Color.parseColor("#2c2c54"))
                .setShadowSize(26f)
                .build();
        arcView.addSeries(seriesBackground);

        //Create data series track
        seriesItem1 = new SeriesItem.Builder(Color.parseColor("#778beb"))
                .setRange(0, targetQty, initialPickedQty)
                .setInitialVisibility(false)
                .setLineWidth(20f)
                .addEdgeDetail(new EdgeDetail(EdgeDetail.EdgeType.EDGE_OUTER, Color.parseColor("#546de5"), 0.4f))
                .setChartStyle(SeriesItem.ChartStyle.STYLE_DONUT)
                .setShadowColor(Color.argb(100, 200, 200, 200))
                .build();


        series1Index = arcView.addSeries(seriesItem1);
        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(50)
                .setDuration(50)
                .setDisplayText("Display Text")
                .build());


        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                txtview.setText(String.format("%.3f", currentPosition) + "/" + String.format("%.3f", seriesItem1.getMaxValue()));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

    }

    void doScan(int seq) {
        Intent intent = new Intent();

        intent.setAction(Constants.ACTION_BARCODE_SET_TRIGGER);
        intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
        intent.putExtra(Constants.EXTRA_INT_DATA2, 1);
        intent.putExtra(Constants.EXTRA_INT_DATA3, seq);

        btn_id = seq;

        sendBroadcast(intent);

        showScanningDialog(seq);
    }


    FancyGifDialog scanningDialog;

    private void showScanningDialog(final int seq) {
        log("seq:" + seq);

        String type = "";

        if (seq == SEQ_BTN_SCAN)
            type = "Material";
        else if (seq == SEQ_BTN_START_SERIAL)
            type = "Start Serial Number";
        else if (seq == SEQ_BTN_END_SERIAL)
            type = "Last Serial Number";
        else if (seq == SEQ_BTN_SOURCE_BIN)
            type = "Source Bin";
        else if (seq == SEQ_BTN_DEST_BIN)
            type = "Destination Bin";

        final String msg = "Please point the scanner towards the '" + type + "' barcode";

        scanningDialog = showDialog("Scanning...", msg, Dialog_Type.PROMPT,
                "Cancel", () -> {
                    cancelScanning(seq);
                });

        new Handler().postDelayed(() -> dismissScanningDialog(), 5000);
    }

    void cancelScanning(int seq) {
        Intent intent = new Intent();
        intent.setAction(Constants.ACTION_BARCODE_SET_TRIGGER);
        intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
        intent.putExtra(Constants.EXTRA_INT_DATA2, 0);
        intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_SET_TRIGGER_OFF);
        sendBroadcast(intent);

        dismissScanningDialog();
    }

    //
    private void dismissScanningDialog() {
        try {
            if (!this.isFinishing() && scanningDialog != null)
                scanningDialog.dismiss(scanningDialog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnScan)
    void OnClickScan(View vi) {
        doScan(SEQ_BTN_SCAN);
    }

    @OnClick(R.id.btnPost)
    void OnClickPost(View vi) {
        doPost();
    }

    private void doPost() {

        if (!firstScanned) {
            toast("You must scan an Item first", ToastType.ERROR);
            return;
        }

        if (type.equals("RECV") && !isDestBinVerified) {
            verifyDestBin();
            return;
        } else if (type.equals("BIN2BIN") && !isDestBinVerified) {
            verifyDestBin();
            return;
        }

        String currentBatch = "";

        if (isBatchable) {
            currentBatch = edtBatch.getText().toString();

            if (currentBatch.equals(transferOrder.getBatch())) {
                log("Scan Act", "Batch Verified::O Batch->" + transferOrder.getBatch() + ":C Batch->" + currentBatch);
            } else {
                toast("Batch number is not correct", ToastType.INFO);
                return;
            }
        }

         String startSeries = "", endSeries = "",
                startSeries2 = "", endSeries2 = "",
                startSeries3 = "", endSeries3 = "",
                startSeries4 = "", endSeries4 = "",
                startSeries5 = "", endSeries5 = "";
        if (transferOrder.getIsSerialized().equals("X") && !type.equals("BIN2BIN")) {
            startSeries = edtSeriesStart.getText().toString();
            endSeries = edtSeriesEnd.getText().toString();
            startSeries2 = edtSeriesStart2.getText().toString();
            endSeries2 = edtSeriesEnd2.getText().toString();
            startSeries3 = edtSeriesStart3.getText().toString();
            endSeries3 = edtSeriesEnd3.getText().toString();
            startSeries4 = edtSeriesStart4.getText().toString();
            endSeries4 = edtSeriesEnd4.getText().toString();
            startSeries5 = edtSeriesStart5.getText().toString();
            endSeries5 = edtSeriesEnd5.getText().toString();

            if(startSeries.equals("") && startSeries2.equals("") && startSeries3.equals("") && startSeries4.equals("") && startSeries5.equals("") == true &&
               endSeries.equals("") && endSeries2.equals("") && endSeries3.equals("") && endSeries4.equals("") && endSeries5.equals("")){
                toast("You must scan atleast one Start and End Serial numbers", ToastType.ERROR);
                return;
            }

            if ((startSeries.equals("") && endSeries.equals("")) == false && (!startSeries.equals("") && !endSeries.equals("")) == false) {
                toast("You must scan both, Start and End Serial numbers for series 1", ToastType.ERROR);
                return;
            }
            if (ll_Series_2.getVisibility() == View.VISIBLE && (!startSeries2.equals("") && !endSeries2.equals("")) == false && (startSeries2.equals("") && endSeries2.equals("")) == false) {
                toast("You must scan both, Start and End Serial numbers for series 2", ToastType.ERROR);
                return;
            }
            if (ll_Series_3.getVisibility() == View.VISIBLE &&(!startSeries3.equals("") && !endSeries3.equals("")) == false && (startSeries3.equals("") && endSeries3.equals("")) == false) {
                toast("You must scan both, Start and End Serial numbers for series 3", ToastType.ERROR);
                return;
            }
            if (ll_Series_4.getVisibility() == View.VISIBLE &&(!startSeries4.equals("") && !endSeries4.equals("")) == false && (startSeries4.equals("") && endSeries4.equals("")) == false) {
                toast("You must scan both, Start and End Serial numbers for series 4", ToastType.ERROR);
                return;
            }
            if (ll_Series_5.getVisibility() == View.VISIBLE &&(!startSeries5.equals("") && !endSeries5.equals("")) == false && (startSeries5.equals("") && endSeries5.equals("")) == false) {
                toast("You must scan both, Start and End Serial numbers for series 5", ToastType.ERROR);
                return;
            }
        }

        String serviceUrl = new UrlProvider().getItemPickingUrl(getWareHouseNumber(),
                getUsername(),
                currentPickedQty - initialPickedQty,
                transferOrder.getTransferOrder(),
                transferOrder.getToItem(),
                startSeries,
                endSeries,
                currentBatch);

        ApiInterface apiService =
                RetrofitInstance.getInstance().create(ApiInterface.class);

        Call<PickItem> call = apiService.pickItemService(serviceUrl);

        FancyGifDialog fancyGifDialog = showDialog("Posting To Server", "Your picking is being processed", Dialog_Type.LOADING);

        call.enqueue(new Callback<PickItem>() {
            @Override
            public void onResponse(Call<PickItem> call, Response<PickItem> response) {
                fancyGifDialog.dismiss(fancyGifDialog);

                if (response.body().getD().getResponseCode().equals("200")) {
                    showDialog("Item Picking Successful!", response.body().getD().getResponseMessage() + "",
                            Dialog_Type.SUCCESS, "Ok", () -> finish());

                } else if (response.body().getD().getResponseCode().equals("500")) {
                    showDialog("Item Picking Unsuccessful!", response.body().getD().getResponseMessage() + "",
                            Dialog_Type.FAILURE, "Ok", () -> finish());

                } else {
                    showDialog("Item Picking Failed!!", response.body().getD().getResponseMessage() + "",
                            Dialog_Type.FAILURE, "Ok");

                }
            }

            @Override
            public void onFailure(Call<PickItem> call, Throwable t) {
                fancyGifDialog.dismiss(fancyGifDialog);

                showDialog("Item Picking Failed due to error!", "Error:" + t.getMessage(),
                        Dialog_Type.FAILURE, "Ok");
            }
        });
    }


//    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//
//            int seq = intent.getIntExtra(Constants.EXTRA_INT_DATA3, 0);
//
//            if (action.equals(Constants.ACTION_BARCODE_CALLBACK_DECODING_DATA)) {
//
//                int handle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
//                byte[] data = intent
//                        .getByteArrayExtra(Constants.EXTRA_BARCODE_DECODING_DATA);
//                String result = "";
//
//                if (data != null) {
//                    result = new String(data);
//                    String btnId = intent.getStringExtra("button_id");
//
//                    log("Btn ID:" + btnId);
//                    toast(btnId);
//
//                    processBarcodedString(result);
//                }
//
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS)) {
//                mBarcodeHandle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
//
//                setSuccessFailText("Success : " + seq);
//
//                if (seq == SEQ_BARCODE_OPEN) {
//                    mCurrentStatus = STATUS_OPEN;
//                } else if (seq == SEQ_BARCODE_CLOSE) {
//                    mCurrentStatus = STATUS_CLOSE;
//                }
//
//
////                hideLoadingDialog();
//
//                refreshCurrentStatus();
//
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_FAILED)) {
//                int result = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
//                if (result == Constants.ERROR_BARCODE_DECODING_TIMEOUT) {
//                    setSuccessFailText("Failed result : " + "Decode Timeout"
//                            + " / seq : " + seq);
//                } else if (result == Constants.ERROR_NOT_SUPPORTED) {
//                    setSuccessFailText("Failed result : " + "Not Supoorted"
//                            + " / seq : " + seq);
//                } else if (result == Constants.ERROR_BARCODE_ERROR_USE_TIMEOUT) {
//                    mCurrentStatus = STATUS_CLOSE;
//                    setSuccessFailText("Failed result : " + "Use Timeout"
//                            + " / seq : " + seq);
//                } else
//                    setSuccessFailText("Failed result : " + result
//                            + " / seq : " + seq);
//                refreshCurrentStatus();
//            } else if (action
//                    .equals(Constants.ACTION_BARCODE_CALLBACK_GET_STATUS)) {
//                int status = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
//
//                switch (status) {
//                    case 0:
//                        mCurrentStatus = STATUS_CLOSE;
//                        break;
//                    case 1:
//                        mCurrentStatus = STATUS_OPEN;
//                        break;
//                    case 2:
//                        mCurrentStatus = STATUS_TRIGGER_ON;
//                        break;
//                }
//                setResultText(mCurrentStatus);
//            }
//        }
//    };

    private void processBarcodedString(String result) {
        if (result != null && result.length() > 0) {
            if (result.contains("-")) {
                String[] results = result.split("-");

                log("splittedArray = " + Arrays.toString(results));

                if (results.length > 0) {
                    String matCode = results[1];
//                    toast(matCode);

                    matCode = matCode.replaceFirst("^0+(?!$)", "");
                    String matNumber = transferOrder.getMaterialNumber().replaceFirst("^0+(?!$)", "");

                    if (matCode.equalsIgnoreCase(matNumber)) {
                        // Update DecoView
                        this.firstScanned = true;
                        vibrate();
//                        updateDecoView();

                        txtview.performClick();

                    } else {
                        toast("Incorrect Material Scanned", ToastType.INFO);
                    }
                }
            }
        } else {
//            toast("Incorrect Material Scanned", ToastType.INFO);
        }
    }

    void updateDecoView() {
        if (currentPickedQty <= targetQty)
            arcView.addEvent(new DecoEvent
                    .Builder(currentPickedQty)
                    .setIndex(series1Index)
                    .setDelay(50)
                    .setDuration(300)
                    .setListener(new DecoEvent.ExecuteEventListener() {
                        @Override
                        public void onEventStart(DecoEvent event) {
                        }
                        @Override
                        public void onEventEnd(DecoEvent event) {
                            if(currentPickedQty == targetQty){
                                seriesItem1.setColor(Color.parseColor("#00802b"));
                            }
                        }
                    })
                    .build());
        else
            toast("You have already picked all items", ToastType.ERROR);

        if(IS_SERIALIZED){
            btn_id = SEQ_BTN_START_SERIAL;
        }
        else if(!type.equals("ISS")){
            btn_id = SEQ_BTN_DEST_BIN;
        }
        else {
            if(Config.triggerPosting == true)
                btn_id = SEQ_BTN_POST;
        }
    }


    @Override
    protected void onResume() {
        barcode_OnResume();

        super.onResume();

        hideKeyboard(this);

    }


    @Override
    protected void onPause() {
        barcode_OnPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        barcode_OnDestroy();
        super.onDestroy();
    }


    //region Barcode related work


    boolean isHBPressed = false;


    private String mCurrentStatus;
    private String mSavedStatus;
    private boolean mIsRegisterReceiver;


    private int mBarcodeHandle = -1;
    private int mCount = 0;
    private String[] STATUS_ARR = {STATUS_CLOSE, STATUS_OPEN, STATUS_TRIGGER_ON};

    private boolean mIsOpened = false;

    private static final String STATUS_CLOSE = "STATUS_CLOSE";
    private static final String STATUS_OPEN = "STATUS_OPEN";
    private static final String STATUS_TRIGGER_ON = "STATUS_TRIGGER_ON";

    private static final int SEQ_BARCODE_OPEN = 100;
    private static final int SEQ_BARCODE_CLOSE = 200;
    private static final int SEQ_BARCODE_GET_STATUS = 300;
    private static final int SEQ_BARCODE_SET_TRIGGER_ON = 400;
    private static final int SEQ_BARCODE_SET_TRIGGER_OFF = 500;
    private static final int SEQ_BARCODE_SET_PARAMETER = 600;
    private static final int SEQ_BARCODE_GET_PARAMETER = 700;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

//        Toast.makeText(this, "KeyCode:" + keyCode, Toast.LENGTH_SHORT).show();

        if (KEYCODES.contains(keyCode)) {
            if (!isHBPressed) {
                isHBPressed = true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

//        Toast.makeText(this, "KeyCodeUp:" + keyCode, Toast.LENGTH_SHORT).show();

        if (KEYCODES.contains(keyCode)) {
            if (isHBPressed) {
                isHBPressed = false;
            }
        }

        return super.onKeyUp(keyCode, event);
    }


    void barcode_OnCreate() {
        mSavedStatus = mCurrentStatus = STATUS_CLOSE;
        mIsRegisterReceiver = false;

        registerReceiver();
    }

    void initScan() {

        Intent intent = new Intent();

        String action = Constants.ACTION_BARCODE_CLOSE;
        int seq = SEQ_BARCODE_CLOSE;

        if (mCurrentStatus.equals(STATUS_CLOSE))
            action = Constants.ACTION_BARCODE_OPEN;

        intent.setAction(action);

        if (mIsOpened)
            intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);

        if (mCurrentStatus.equals(STATUS_CLOSE))
            seq = SEQ_BARCODE_OPEN;

        intent.putExtra(Constants.EXTRA_INT_DATA3, seq);
        sendBroadcast(intent);

        if (mCurrentStatus.equals(STATUS_CLOSE)) {
            mIsOpened = true;
//            // setResultText("BARCODE_OPEN");
//             // showProgressDialog(true);
        } else {
            mIsOpened = false;
//            // setResultText("BARCODE_CLOSE");
        }
    }

    void barcode_OnResume() {

        registerReceiver();
        resetCurrentView();


        initScan();
    }

    void barcode_OnPause() {
        mSavedStatus = mCurrentStatus;
        Intent intent = new Intent();
        intent.setAction(Constants.ACTION_BARCODE_CLOSE);
        intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
        intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_CLOSE);
        sendBroadcast(intent);
        unregisterReceiver();
        mCurrentStatus = STATUS_CLOSE;
    }

    private void registerReceiver() {
        if (mIsRegisterReceiver) return;
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_DECODING_DATA);
        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS);
        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_REQUEST_FAILED);
        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_PARAMETER);
        filter.addAction(Constants.ACTION_BARCODE_CALLBACK_GET_STATUS);

        registerReceiver(mReceiver, filter);
        mIsRegisterReceiver = true;
    }


    private void resetCurrentView() {
        if (!mSavedStatus.equals(mCurrentStatus)) {
            if (!mSavedStatus.equals(STATUS_CLOSE)) {
                Intent intent = new Intent();
                intent.setAction(Constants.ACTION_BARCODE_OPEN);
                intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
                intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_OPEN);
                sendBroadcast(intent);
                // showProgressDialog(true);
                return;
            }
        }
        // refreshCurrentStatus();
        // refreshButton();
    }

    private void unregisterReceiver() {
        if (!mIsRegisterReceiver) return;
        unregisterReceiver(mReceiver);
        mIsRegisterReceiver = false;
    }


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            int handle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
            int seq = intent.getIntExtra(Constants.EXTRA_INT_DATA3, 0);

            if (action.equals(Constants.ACTION_BARCODE_CALLBACK_DECODING_DATA)) {
                mCount++;
                byte[] data = intent.getByteArrayExtra(Constants.EXTRA_BARCODE_DECODING_DATA);
                int symbology = intent.getIntExtra(Constants.EXTRA_INT_DATA2, -1);

                String result = "";
//                result = "[BarcodeDecodingData handle : " + handle + " / count : " + mCount + " / seq : " + seq + "]\n";
//                result += ("[Symbology] : " + symbology + "\n");
                String dataResult = "";

                if (data != null) {
                    result = new String(data);

                    dismissScanningDialog();

                    if (btn_id == SEQ_BTN_DEST_BIN) {
                        // Dest Bin Scan
                        if (checkAndVerifyBin(result, transferOrder.getDestBin())) {
                            isDestBinVerified = true;
                            try {
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            vibrate();
                            toast("Posting data", ToastType.INFO);
                            if(Config.triggerPosting == true)
                                btn_id = SEQ_BTN_POST;
                        }

                    } else if (btn_id == SEQ_BTN_SOURCE_BIN) {
                        // Source Bin Scan
                        isSourceBinVerified = true;
                        if (checkAndVerifyBin(result, transferOrder.getSourceBin())) {
                            try {
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            vibrate();
                            btn_id = SEQ_BTN_SCAN;
                        }


                    } else if (btn_id == SEQ_BTN_START_SERIAL) {
                        // Start Serial.
                        SeriesScanned++;
                        vibrate();

                        switch(SeriesScanned){
                            case 1:
                                edtSeriesStart.setText(result);
                                break;
                            case 2:
                                edtSeriesStart2.setText(result);
                                break;
                            case 3:
                                edtSeriesStart3.setText(result);
                                break;
                            case 4:
                                edtSeriesStart4.setText(result);
                                break;
                            case 5:
                                edtSeriesStart5.setText(result);
                                break;
                        }

                        btn_id = SEQ_BTN_END_SERIAL;

                    } else if (btn_id == SEQ_BTN_END_SERIAL) {
                        // End Serial
                        vibrate();
                        switch(SeriesScanned){
                            case 1:
                                edtSeriesEnd.setText(result);
                                break;
                            case 2:
                                edtSeriesEnd2.setText(result);
                                break;
                            case 3:
                                edtSeriesEnd3.setText(result);
                                break;
                            case 4:
                                edtSeriesEnd4.setText(result);
                                break;
                            case 5:
                                edtSeriesEnd5.setText(result);
                                break;
                        }

                        if(SeriesScanned<SeriesCount){
                            btn_id = SEQ_BTN_START_SERIAL;
                        }
                        else if(!type.equals("ISS")){
                            btn_id = SEQ_BTN_DEST_BIN;
                        }
                        else {
                            if(Config.triggerPosting == true)
                                btn_id = SEQ_BTN_POST;
                        }

                    } else if (btn_id == SEQ_BTN_SCAN) {
                        // Normal Material Scan
                        processBarcodedString(result);

                    } else if (btn_id == SEQ_BTN_POST) {
                        doPost();

                    }

                }

                result += "[Data] : " + dataResult;
                // setResultText(result);
            } else if (action.equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS)) {
                try {
                    dismissScanningDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // setSuccessFailText("Success : " + seq);
                if (seq == SEQ_BARCODE_OPEN) {
                    mBarcodeHandle = intent.getIntExtra(Constants.EXTRA_HANDLE, 0);
                    mCurrentStatus = STATUS_OPEN;
                    // showProgressDialog(false);
                } else if (seq == SEQ_BARCODE_CLOSE) {
                    mCurrentStatus = STATUS_CLOSE;
                    // showProgressDialog(false);
                } else if (seq == SEQ_BARCODE_GET_STATUS) {
                    mCurrentStatus = STATUS_CLOSE;
                    // showProgressDialog(false);
                } else if (seq == SEQ_BARCODE_SET_TRIGGER_ON) mCurrentStatus = STATUS_TRIGGER_ON;
                else if (seq == SEQ_BARCODE_SET_TRIGGER_OFF) mCurrentStatus = STATUS_OPEN;
                else if (seq == SEQ_BARCODE_SET_PARAMETER) {
                    // setResultText("SET_PARAMETER success");
                } else {
                    // showProgressDialog(false);
                }

                // refreshCurrentStatus();
                // refreshButton();
            } else if (action.equals(Constants.ACTION_BARCODE_CALLBACK_REQUEST_FAILED)) {
                try {
                    dismissScanningDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int result = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
                // showProgressDialog(false);
                if (result == Constants.ERROR_BARCODE_DECODING_TIMEOUT) {
                    // setSuccessFailText("Failed result : " + "Decode Timeout" + " / seq : " + seq);
                } else if (result == Constants.ERROR_NOT_SUPPORTED) {
                    // setSuccessFailText("Failed result : " + "Not Supoorted" + " / seq : " + seq);
                } else if (result == Constants.ERROR_BARCODE_ERROR_USE_TIMEOUT) {
                    mCurrentStatus = STATUS_CLOSE;
                    // setSuccessFailText("Failed result : " + "Use Timeout" + " / seq : " + seq);
                } else if (result == Constants.ERROR_BARCODE_ERROR_ALREADY_OPENED) {
                    mCurrentStatus = STATUS_OPEN;
                    // setSuccessFailText("Failed result : " + "Already opened" + " / seq : " + seq);
                } else if (result == Constants.ERROR_BATTERY_LOW) {
                    mCurrentStatus = STATUS_CLOSE;
                    // setSuccessFailText("Failed result : " + "Battery low" + " / seq : " + seq);
                } else if (result == Constants.ERROR_NO_RESPONSE) {
                    int notiCode = intent.getIntExtra(Constants.EXTRA_INT_DATA3, 0);
                    // setSuccessFailText("Failed result : " + notiCode + "/ ### ERROR_NO_RESPONSE ###");
                    mCurrentStatus = STATUS_CLOSE;
                    // setSuccessFailText("Failed result : " + result + " / seq : " + seq);
                } else {
                    // setSuccessFailText("Failed result : " + result + " / seq : " + seq);
                }
                if (seq == SEQ_BARCODE_SET_PARAMETER) {
                    if (result == Constants.ERROR_BARCODE_EXCEED_ASCII_CODE) {
                        // setResultText("SET_PARAMETER failed:exceed range of ascii code");
                    }
                }
                // refreshCurrentStatus();
                // refreshButton();
            } else if (action.equals(Constants.ACTION_BARCODE_CALLBACK_PARAMETER)) {
                try {
                    dismissScanningDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int parameter = intent.getIntExtra(Constants.EXTRA_INT_DATA2, -1);
                String value = intent.getStringExtra(Constants.EXTRA_STR_DATA1);

                // setResultText("Get parameter result\nparameter : " + parameter + " / value : " + value);
            } else if (action.equals(Constants.ACTION_BARCODE_CALLBACK_GET_STATUS)) {
                try {
                    dismissScanningDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int status = intent.getIntExtra(Constants.EXTRA_INT_DATA2, 0);
                mCurrentStatus = STATUS_ARR[status];
                // setResultText("Current Status : " + mCurrentStatus + " / id : " + status);
                // refreshCurrentStatus();
            }
        }
    };

    private boolean checkAndVerifyBin(String result, String binNumber) {

        try {
//        if (result.length() == 18) { // Valid Bin type
            String warehouse = result.substring(0, 3);
            String storageType = result.substring(4, 7);
            String bin = result.substring(8);

            if (warehouse.equals(getWareHouseNumber())) {
//                if (storageType.equals(getStorageType())) {
                if (bin.equals(binNumber)) {
                    // All are valid, bin verified...
                    return true;

                } else {
                    toast("Wrong BIN", ToastType.ERROR);
                }
//                } else {
//                    toast("Wrong Storage Type", ToastType.ERROR);
//                }
            } else {
                toast("Wrong Warehouse", ToastType.ERROR);
            }
//        }

            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    //must have to close when app destroyed
    private void barcode_OnDestroy() {
        if (!mCurrentStatus.equals(STATUS_CLOSE)) {
            Intent intent = new Intent();
            intent.setAction(Constants.ACTION_BARCODE_CLOSE);
            intent.putExtra(Constants.EXTRA_HANDLE, mBarcodeHandle);
            intent.putExtra(Constants.EXTRA_INT_DATA3, SEQ_BARCODE_CLOSE);
            sendBroadcast(intent);
        }
//        if(mProgressDialog!=null) mProgressDialog.dismiss();
//        mProgressDialog = null;
        unregisterReceiver();
//        stopProgressTimer();
    }

    public void addSeries(View view) {
        SeriesCount++;

           switch (SeriesCount) {
                case 2:
                    ll_Series_2.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    ll_Series_3.setVisibility(View.VISIBLE);
                    break;
                case 4:
                    ll_Series_4.setVisibility(View.VISIBLE);
                    break;
                case 5:
                    ll_Series_5.setVisibility(View.VISIBLE);
                    addBtn.setVisibility(View.GONE);
                    break;
            }

    }


    public static class Constants {

        public static Integer[] KEYCODES_IntArray = new Integer[]{280, 281};
        public static List<Integer> KEYCODES = Arrays.asList(KEYCODES_IntArray);

        public static final String ACTION_BARCODE_OPEN = "kr.co.bluebird.android.bbapi.action.BARCODE_OPEN";
        public static final String ACTION_BARCODE_CLOSE = "kr.co.bluebird.android.bbapi.action.BARCODE_CLOSE";
        public static final String ACTION_BARCODE_SET_TRIGGER = "kr.co.bluebird.android.bbapi.action.BARCODE_SET_TRIGGER";
        public static final String ACTION_BARCODE_CALLBACK_REQUEST_SUCCESS = "kr.co.bluebird.android.bbapi.action.BARCODE_CALLBACK_REQUEST_SUCCESS";
        public static final String ACTION_BARCODE_CALLBACK_REQUEST_FAILED = "kr.co.bluebird.android.bbapi.action.BARCODE_CALLBACK_REQUEST_FAILED";
        public static final String ACTION_BARCODE_CALLBACK_DECODING_DATA = "kr.co.bluebird.android.bbapi.action.BARCODE_CALLBACK_DECODING_DATA";
        public static final String ACTION_BARCODE_SET_PARAMETER = "kr.co.bluebird.android.bbapi.action.BARCODE_SET_PARAMETER";
        public static final String ACTION_BARCODE_GET_PARAMETER = "kr.co.bluebird.android.bbapi.action.BARCODE_GET_PARAMETER";
        public static final String ACTION_BARCODE_CALLBACK_PARAMETER = "kr.co.bluebird.android.bbapi.action.BARCODE_CALLBACK_PARAMETER";
        public static final String ACTION_BARCODE_GET_STATUS = "kr.co.bluebird.android.action.BARCODE_GET_STATUS";
        public static final String ACTION_BARCODE_CALLBACK_GET_STATUS = "kr.co.bluebird.android.action.BARCODE_CALLBACK_GET_STATUS";


        public static final String EXTRA_BARCODE_DECODING_DATA = "EXTRA_BARCODE_DECODING_DATA";
        public static final String EXTRA_HANDLE = "EXTRA_HANDLE";
        public static final String EXTRA_INT_DATA2 = "EXTRA_INT_DATA2";
        public static final String EXTRA_STR_DATA1 = "EXTRA_STR_DATA1";
        public static final String EXTRA_INT_DATA3 = "EXTRA_INT_DATA3";


        public static final int ERROR_FAILED = -1;
        public static final int ERROR_NOT_SUPPORTED = -2;
        public static final int ERROR_NO_RESPONSE = -4;
        public static final int ERROR_BATTERY_LOW = -5;
        public static final int ERROR_BARCODE_DECODING_TIMEOUT = -6;
        public static final int ERROR_BARCODE_ERROR_USE_TIMEOUT = -7;
        public static final int ERROR_BARCODE_ERROR_ALREADY_OPENED = -8;
        public static final int ERROR_BARCODE_EXCEED_ASCII_CODE = -10;
    }

    //endregion
}

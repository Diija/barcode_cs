package pk.com.ke.barcode_cs.adapter;

import android.graphics.Typeface;
import android.support.annotation.CallSuper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhukic.sectionedrecyclerview.SectionedRecyclerViewAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.api.models.opento.Result;

/**
 * @author Vladislav Zhukov (https://github.com/zhukic)
 */

public abstract class BaseSectionAdapter extends SectionedRecyclerViewAdapter<BaseSectionAdapter.SubheaderHolder, BaseSectionAdapter.transferOrderViewHolder> {

    public interface OnItemClickListener {
        void onItemClicked(Result movie,int position);
        void onSubheaderClicked(int position);
    }

    List<Result> transferOrderList;

    OnItemClickListener onItemClickListener;

    static class SubheaderHolder extends RecyclerView.ViewHolder {

        private static Typeface meduiumTypeface = null;

        TextView mSubheaderText;
        ImageView mArrow;

        SubheaderHolder(View itemView) {
            super(itemView);
            this.mSubheaderText = (TextView) itemView.findViewById(R.id.subheaderText);
            this.mArrow = (ImageView) itemView.findViewById(R.id.arrow);

//            if(meduiumTypeface == null) {
//                meduiumTypeface = Typeface.createFromAsset(itemView.getContext().getAssets(), "Roboto-Medium.ttf");
//            }
//            this.mSubheaderText.setTypeface(meduiumTypeface);
        }

    }

    static class transferOrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtTransferOrder) TextView textTransferOrder;
        @BindView(R.id.txtMaterialCode) TextView textMaterialCode;
        @BindView(R.id.vAssignmentIndicater) View viewAssignmentIndicator;
        @BindView(R.id.txtBadge) TextView textBadge;
        @BindView(R.id.txtSource)  TextView textSource;
        @BindView(R.id.txtDestination) TextView texDestination;
        @BindView(R.id.txtQuantity) TextView textQuantity;

        transferOrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            this.textMovieName = (TextView) itemView.findViewById(R.id.movieName);
//            this.textMovieGenre = (TextView) itemView.findViewById(R.id.movieGenre);
//            this.textMovieYear = (TextView) itemView.findViewById(R.id.movieYear);
//            this.PONumber = (TextView) itemView.findViewById(R.id.lblpToNumber);


        }
    }

    BaseSectionAdapter(List<Result> itemList) {
        super();
        this.transferOrderList = itemList;
    }

    @Override
    public transferOrderViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        return new transferOrderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false));
    }

    @Override
    public SubheaderHolder onCreateSubheaderViewHolder(ViewGroup parent, int viewType) {
        return new SubheaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false));
    }

    @Override
    @CallSuper
    public void onBindSubheaderViewHolder(SubheaderHolder subheaderHolder, int nextItemPosition) {

        boolean isSectionExpanded = isSectionExpanded(getSectionIndex(subheaderHolder.getAdapterPosition()));

        if (isSectionExpanded) {
            subheaderHolder.mArrow.setImageDrawable(ContextCompat.getDrawable(subheaderHolder.itemView.getContext(), R.drawable.ic_keyboard_arrow_up_black_24dp));
        } else {
            subheaderHolder.mArrow.setImageDrawable(ContextCompat.getDrawable(subheaderHolder.itemView.getContext(), R.drawable.ic_keyboard_arrow_down_black_24dp));
        }

//        subheaderHolder.itemView.setOnClickListener(v -> onItemClickListener.onSubheaderClicked(subheaderHolder.getAdapterPosition()));

    }

    @Override
    public int getItemSize() {
        return transferOrderList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}

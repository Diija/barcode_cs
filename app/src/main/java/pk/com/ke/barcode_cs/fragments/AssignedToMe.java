package pk.com.ke.barcode_cs.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shashank.sony.fancygifdialoglib.FancyGifDialog;

import org.parceler.Parcels;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.DecoTestActivity;
import pk.com.ke.barcode_cs.activities.HomeActivity;
import pk.com.ke.barcode_cs.activities.ScanActivity;
import pk.com.ke.barcode_cs.adapter.BaseSectionAdapter;
import pk.com.ke.barcode_cs.adapter.RecyclerAdapter;
import pk.com.ke.barcode_cs.api.models.opento.Result;


public class AssignedToMe extends Fragment implements HomeActivity.FragmentDataProvider, BaseSectionAdapter.OnItemClickListener ,HomeActivity.FragmentItemUpdate {

    RecyclerAdapter mSectionedRecyclerAdapter;
    private HomeActivity.FragmentItemUpdate mListener;
    private RecyclerView recyclerView;
    List<Result> results;
    public final String REQUESTTAG = "Assignment";


    public AssignedToMe() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_assigned_to_me, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = getActivity().findViewById(R.id.recyclerView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof HomeActivity.FragmentItemUpdate) {
//            mListener = (HomeActivity.FragmentItemUpdate) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement HomeActivity.FragmentItemUpdate");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void providedData(List<Result> results) {

        if (isAdded()) {

            this.results = results;
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            Comparator<Result> movieComparator = (o1, o2) -> o1.getToItem().compareTo(o2.getToItem());
            Collections.sort(results, movieComparator);
            mSectionedRecyclerAdapter = new RecyclerAdapter(getActivity(), results);

            mSectionedRecyclerAdapter.setOnItemClickListener(this);
            recyclerView.setAdapter(mSectionedRecyclerAdapter);
        }
    }


    public void onItemClicked(Result transferOrder, int position) {
//        Toast.makeText(getActivity(), "Item Clicked:" + movie.getToItem(), Toast.LENGTH_SHORT).show();
    //    FancyToast.makeText(getActivity(),transferOrder.getMaterialDescription(),FancyToast.LENGTH_LONG,FancyToast.SUCCESS,false).show();
        if(transferOrder.getToUser().equals("")){

            @SuppressLint("ResourceType") FancyGifDialog fm = new FancyGifDialog.Builder(getActivity())
                    .setTitle("Assign Transfer Order Item To me")
                    .setMessage("Would you like to assign this Transfer Order item to yourself")
                    .setNegativeBtnText("No")
                    .setPositiveBtnBackground(getResources().getString(R.color.badgecolor))
                    .setPositiveBtnText("Yes")
                    .setNegativeBtnBackground(getResources().getString(R.color.colorPrimaryDark))
                    .setGifResource(R.drawable.gif1)   //Pass your Gif here
                    .isCancellable(true)
                    .OnPositiveClicked(() -> {
                        makeAssignment(transferOrder,position);
                        //TODO: what to do if assignement fails
                        transferOrder.setToUser(((HomeActivity)getActivity()).getUsername());
                        mSectionedRecyclerAdapter.notifyItemChanged(position,transferOrder);
                        mSectionedRecyclerAdapter.notifyDataChanged();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
                        ((HomeActivity)getActivity()).intent(ScanActivity.class,bundle,false);

                    })
                    .OnNegativeClicked(()->{})
                    .build();
        }
        else if(transferOrder.getToUser().equals(((HomeActivity)getActivity()).getUsername())){
           //TODO: intent to scanning activity.
            Bundle bundle = new Bundle();
            bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
            ((HomeActivity)getActivity()).intent(DecoTestActivity.class,bundle,false);

        }
        else if(!transferOrder.getToUser().equals(((HomeActivity)getActivity()).getUsername())){
            @SuppressLint("ResourceType") FancyGifDialog fm = new FancyGifDialog.Builder(getActivity())
                    .setTitle("Transfer Order Item Already Assigned")
                    .setMessage("This Transfer Order Item is already assigned to a different user.")
                    .setPositiveBtnBackground(getResources().getString(R.color.Tint1))
                    .setPositiveBtnText("OK")
                    .setGifResource(R.drawable.gif_success)   //Pass your Gif here
                    .isCancellable(true)
                    .OnNegativeClicked(null)
                    .OnPositiveClicked(() -> {
                    })
                    .build();
        }

    }

    @Override
    public void onSubheaderClicked(int position) {
        if (mSectionedRecyclerAdapter.isSectionExpanded(mSectionedRecyclerAdapter.getSectionIndex(position))) {
            mSectionedRecyclerAdapter.collapseSection(mSectionedRecyclerAdapter.getSectionIndex(position));
        } else {
            mSectionedRecyclerAdapter.expandSection(mSectionedRecyclerAdapter.getSectionIndex(position));
        }
    }

    public void updateItem(Result transferOrder, int position, String requestTag) {
        if (requestTag.equals(this.REQUESTTAG)) {
            mSectionedRecyclerAdapter.notifyItemChanged(position, transferOrder);

            transferOrder.setToUser(((HomeActivity) getActivity()).getUsername());
            mSectionedRecyclerAdapter.notifyItemChanged(position, transferOrder);
            mSectionedRecyclerAdapter.notifyDataChanged();
            Bundle bundle = new Bundle();
            bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
            ((HomeActivity) getActivity()).intent(DecoTestActivity.class, bundle, false);
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void makeAssignment(Result transferOrder, int position){
        ((HomeActivity)getActivity()).assignTo(
                ((HomeActivity)getActivity()).getUsername(),
                ((HomeActivity)getActivity()).getWareHouseNumber(),
                transferOrder.getTransferOrder(),
                transferOrder.getToItem(),
                transferOrder,
                position
                ,REQUESTTAG);
    }
}

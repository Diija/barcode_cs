package pk.com.ke.barcode_cs.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.base.BaseActivity;
import pk.com.ke.barcode_cs.api.ApiInterface;
import pk.com.ke.barcode_cs.api.RetrofitInstance;
import pk.com.ke.barcode_cs.api.UrlProvider;
import pk.com.ke.barcode_cs.api.models.login.LoginModel;
import pk.com.ke.barcode_cs.api.models.opento.OpenTOModel;
import pk.com.ke.barcode_cs.api.models.toassignment.TOAssignmentModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class Login extends BaseActivity implements BaseActivity.PermissionCallback {

    // UI references.
    @BindView(R.id.email)
    public AutoCompleteTextView mEmailView;
    @BindView(R.id.password)
    public EditText mPasswordView;
    @BindView(R.id.login_progress)
    public View mProgressView;
    @BindView(R.id.login_form)
    public View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);

        getPermission(this);

        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    mEmailView.setText(s);
                    mEmailView.setSelection(mEmailView.getText().length());
                }
            }
        });

        mPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    mPasswordView.setText(s);
                    mPasswordView.setSelection(mPasswordView.getText().length());
                }
            }
        });
    }


    //Login Handler
    private void attemptLogin() {
        //TODO: change to proper function
        String serviceUrl = new UrlProvider().getLoginUrl(mEmailView.getText().toString(), mPasswordView.getText().toString(),
//                "AMMAR_IS_VERY_COOL_BOI"
                getImei()
        );
//        String serviceUrl = new UrlProvider().getLoginUrl("AMMAR","AMMAR_COOL_BOI","AMMAR_IS_VERY_COOL_BOI");

        ApiInterface apiService =
                RetrofitInstance.getInstance().create(ApiInterface.class);

        Call<LoginModel> call = apiService.Login(serviceUrl);
        showLoadingDialog();

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                hideLoadingDialog();
                if (response != null && response.body() != null) {
                    LoginModel loginmodel = response.body();
                    if (loginmodel.getD().getResponseCode().equals("200")) {

                        storeloginCreadentials(loginmodel);
                        toast(loginmodel.getD().getExMessage(), ToastType.SUCCESS);
                        FancyToast.makeText(getApplicationContext(), loginmodel.getD().getExMessage(), FancyToast.LENGTH_LONG, FancyToast.SUCCESS, true);

                        logUser(loginmodel.getD().getWarehouseNumber() + ":" + loginmodel.getD().getStorageType(),
                                loginmodel.getD().getUsername(),
                                loginmodel.getD().getUsername());

                        intent(HomeActivity.class, true);
                    } else {
//                        Toast.makeText(getApplicationContext(), loginmodel.getD().getResponseMessage(), Toast.LENGTH_LONG).show();
                        toast(loginmodel.getD().getExMessage(), ToastType.ERROR);
                    }
                }
            }

            private void logUser(String s, String username, String username1) {
                // TODO: Use the current user's information
                // You can call any combination of these three methods
                Crashlytics.setUserIdentifier(s);
                Crashlytics.setUserEmail(username);
                Crashlytics.setUserName(username1);
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                // Log error here since request failed
                hideLoadingDialog();
                Log.e("Service Error", t.toString());
//                FancyToast.makeText(getApplicationContext(), "Login service failed", FancyToast.LENGTH_LONG, FancyToast.ERROR, true);
                toast("Login service failed:"+t.getMessage(), ToastType.ERROR);
            }
        });

    }

    @Override
    public void onPermissionGranted(List<String> permission) {
        // Set up the login form.
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    public void onPermissionDenied(List<String> permission) {
        showDialog("Permissions", "You must allow all permissions to use this app", Dialog_Type.FAILURE, "Retry", () -> {
            getPermission(this);
        });
    }
}


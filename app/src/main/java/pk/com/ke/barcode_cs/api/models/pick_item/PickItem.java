
package pk.com.ke.barcode_cs.api.models.pick_item;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickItem implements Serializable, Parcelable
{

    @SerializedName("d")
    @Expose
    private D d;
    public final static Creator<PickItem> CREATOR = new Creator<PickItem>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PickItem createFromParcel(Parcel in) {
            return new PickItem(in);
        }

        public PickItem[] newArray(int size) {
            return (new PickItem[size]);
        }

    }
    ;
    private final static long serialVersionUID = 6329761736476766367L;

    protected PickItem(Parcel in) {
        this.d = ((D) in.readValue((D.class.getClassLoader())));
    }

    public PickItem() {
    }

    public D getD() {
        return d;
    }

    public void setD(D d) {
        this.d = d;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(d);
    }

    public int describeContents() {
        return  0;
    }

}

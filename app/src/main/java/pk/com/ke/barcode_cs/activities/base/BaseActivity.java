package pk.com.ke.barcode_cs.activities.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.parceler.Parcels;

import java.util.Arrays;
import java.util.List;

import pk.com.ke.barcode_cs.Config;
import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.HomeActivity;
import pk.com.ke.barcode_cs.activities.ScanActivity;
import pk.com.ke.barcode_cs.api.RetrofitInstance;
import pk.com.ke.barcode_cs.api.models.login.LoginModel;
import pk.com.ke.barcode_cs.utils.SecurePref;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static pk.com.ke.barcode_cs.Config.IS_LOG_ALLOWED;

/**
 * Created by iqbal.nadeem on 8/20/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    Dialog loadingDialog;
    Retrofit retrofit;
    PermissionCallback permissionCallback;

    final String[] PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.addLogAdapter(new AndroidLogAdapter());

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    public void showLoadingDialog() {
        if (!isFinishing()) { // Activity is not finishing
            loadingDialog = new Dialog(this, R.style.transparent_dialog_borderless);

            loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            loadingDialog.setContentView(R.layout.dialog_progress);
            loadingDialog.setCancelable(false);
            loadingDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            loadingDialog.show();
        }
    }

    public void hideLoadingDialog() {
        if (!isFinishing() && loadingDialog.isShowing()) { // Activity is not finished
            loadingDialog.hide();
        }
    }

    public Retrofit getRetrofit() {
        return retrofit = RetrofitInstance.getInstance();
    }


    public void intent(Class<?> classToLoad) {
        intent(classToLoad, null);
    }

    public void intent(Class<?> classToLoad, boolean finishCurrentActivity) {
        intent(classToLoad, null, finishCurrentActivity);
    }

    public void intent(Class<?> classToLoad, Bundle b) {
        intent(classToLoad, b, false);
    }

    public void intent(Class<?> classToLoad, Bundle b, boolean finishCurrentActivity) {
        Intent i = new Intent(getApplicationContext(), classToLoad);
        if (b != null) {
            i.putExtras(b);
        }
        startActivity(i);

        if (finishCurrentActivity) {
            finish();
        }
    }

    //SharedPreferences functions
    public static final int KEY_TYPE_STRING = 0;
    public static final int KEY_TYPE_INT = 1;
    public static final int KEY_TYPE_FLOAT = 2;
    public static final int KEY_TYPE_BOOLEAN = 3;

    public SecurePref getSharedPreferencesInstance() {
        return new SecurePref(getApplicationContext(), getSharedPreferences("SP", MODE_PRIVATE));
    }

    public void storeToSharedPreference(String key, Object value, int type) {

        SecurePref sharedPreferences = getSharedPreferencesInstance();
        switch (type) {
            case KEY_TYPE_STRING: {
                sharedPreferences.edit().putString(key, (String) value).apply();
                break;

            }
            case KEY_TYPE_INT: {
                sharedPreferences.edit().putInt(key, (int) value).apply();
                break;

            }
            case KEY_TYPE_FLOAT: {
                sharedPreferences.edit().putFloat(key, (float) value).apply();
                break;

            }
            case KEY_TYPE_BOOLEAN: {
                sharedPreferences.edit().putBoolean(key, (Boolean) value).apply();
                break;
            }
        }

    }

    public Object getFromSharedPreference(String key, int type) {
        SecurePref sharedPreferences = getSharedPreferencesInstance();
        switch (type) {
            case KEY_TYPE_STRING: {
                return sharedPreferences.getString(key, null);

            }
            case KEY_TYPE_INT: {
                return sharedPreferences.getInt(key, 0);

            }
            case KEY_TYPE_FLOAT: {
                return sharedPreferences.getFloat(key, 0);

            }
            case KEY_TYPE_BOOLEAN:
                return sharedPreferences.getBoolean(key, false);
            default:
                return 0;
        }
    }

    public void storeloginCreadentials(LoginModel loginmodel) {
        //store username
        storeToSharedPreference(Config.SP.USER_NAME, loginmodel.getD().getUsername(), KEY_TYPE_STRING);
        //store password
        storeToSharedPreference(Config.SP.PASSWORD, loginmodel.getD().getPassword(), KEY_TYPE_STRING);
        //store imei
        storeToSharedPreference(Config.SP.IMEI, loginmodel.getD().getImei(), KEY_TYPE_STRING);
        //store whnumber
        storeToSharedPreference(Config.SP.WAREHOUSE_NUMBER, loginmodel.getD().getWarehouseNumber(), KEY_TYPE_STRING);
        //store storagetyoe
        storeToSharedPreference(Config.SP.STORAGE_TYPE, loginmodel.getD().getStorageType(), KEY_TYPE_STRING);
    }

    public void removeloginCreadentials() {
        //store username
        storeToSharedPreference(Config.SP.USER_NAME, "", KEY_TYPE_STRING);
        //store password
        storeToSharedPreference(Config.SP.PASSWORD, "", KEY_TYPE_STRING);
        //store imei
        storeToSharedPreference(Config.SP.IMEI, "", KEY_TYPE_STRING);
        //store whnumber
        storeToSharedPreference(Config.SP.WAREHOUSE_NUMBER, "", KEY_TYPE_STRING);
        //store storagetyoe
        storeToSharedPreference(Config.SP.STORAGE_TYPE, "", KEY_TYPE_STRING);
    }

    public String getUsername() {
        return (String) getFromSharedPreference(Config.SP.USER_NAME, KEY_TYPE_STRING);
    }

    public String getStorageType() {
        return (String) getFromSharedPreference(Config.SP.STORAGE_TYPE, KEY_TYPE_STRING);
    }

    public String getWareHouseNumber() {
        return (String) getFromSharedPreference(Config.SP.WAREHOUSE_NUMBER, KEY_TYPE_STRING);
    }


    @SuppressLint("MissingPermission")
    public String getImei() {
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public void getPermission(PermissionCallback permissionCallback) {
        if (permissionCallback != null) {
            this.permissionCallback = permissionCallback;
            if (EasyPermissions.hasPermissions(this, PERMISSIONS)) {
                permissionCallback.onPermissionGranted(Arrays.asList(PERMISSIONS));
            } else {
                EasyPermissions.requestPermissions(this, "Permissions are requried by this application to function correctly", 200, PERMISSIONS);
            }
        } else {
            throw new NullPointerException("Permission Callback can't be null");
        }
    }

    //LOGGING
    public void log(int text) {
        log("" + text);
    }

    public void log(long text) {
        log("" + text);
    }

    public void log(double text) {
        log("" + text);
    }

    public void log(float text) {
        log("" + text);
    }

    public void log(String text) {
        log("" + getApplicationContext().getClass().getSimpleName(), "" + text);
    }

    public void log(int key, String text) {
        log("" + key, "" + text);
    }

    public void log(long key, int text) {
        log("" + key, "" + text);
    }

    public void log(float key, int text) {
        log("" + key, "" + text);
    }

    public void log(double key, int text) {
        log("" + key, "" + text);
    }

    public void log(String key, int text) {
        log("" + key, "" + text);
    }

    public void log(String key, long text) {
        log("" + key, "" + text);
    }

    public void log(String key, double text) {
        log("" + key, "" + text);
    }

    public void log(String key, float text) {
        log("" + key, "" + text);
    }

    public static void log(String key, String text) {
        if (IS_LOG_ALLOWED) {
            Log.e(key, text);
        }

        if (text != null && text.length() > 97) {
            text = text.substring(0, 97) + "...";
        }

        if (key != null && key.length() > 97) {
            key = key.substring(0, 97) + "...";
        }

//        try {
//            Bugfender.e(key, text);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Answers.getInstance().logContentView(new ContentViewEvent()
//                .putContentName("LOG")
//                .putContentType("Log")
//                .putContentId("UserID:" + getString(StringUtils.PREF_USER_USER_ID))
//                .putCustomAttribute("Key", "" + key)
//                .putCustomAttribute("Msg", "" + text));

    }

    public void toast(String text) {
        if (text != null && !TextUtils.isEmpty(text)) {
            Toast.makeText(this, "" + text, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "null", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        if (perms.size() == PERMISSIONS.length) {
            // All permissions granted
            if (permissionCallback != null) {
                permissionCallback.onPermissionGranted(perms);
            }
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

        if (permissionCallback != null) {
            permissionCallback.onPermissionDenied(perms);
        }
    }

    public interface PermissionCallback {
        void onPermissionGranted(List<String> permission);

        void onPermissionDenied(List<String> permission);
    }

    public enum ToastType {
        SUCCESS,
        ERROR,
        INFO
    }

    public void toast(String text, ToastType toastType) {
        if (toastType.equals(ToastType.SUCCESS)) {
            FancyToast.makeText(this, text, Toast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();
        } else if (toastType.equals(ToastType.INFO)) {
            FancyToast.makeText(this, text, Toast.LENGTH_SHORT, FancyToast.INFO, false).show();
        } else if (toastType.equals(ToastType.ERROR)) {
            FancyToast.makeText(this, text, Toast.LENGTH_SHORT, FancyToast.ERROR, false).show();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public enum Dialog_Type {
        LOADING,
        SUCCESS,
        FAILURE,
        PROMPT
    }


    public FancyGifDialog showDialog(String title, String msg, Dialog_Type dialog) {
        return showDialog(title, msg, dialog, null, null);
    }

    public FancyGifDialog showDialog(String title, String msg, Dialog_Type dialog, String positiveText) {
        return showDialog(title, msg, dialog, positiveText, () -> {

        });
    }

    public FancyGifDialog showDialog(String title, String msg, Dialog_Type dialog, String positiveText, FancyGifDialogListener positiveTextClick) {
        return showDialog(title, msg, dialog, positiveText, positiveTextClick, null, null);
    }

    public FancyGifDialog showDialog(String title, String msg, Dialog_Type dialog, String positiveBtnText, FancyGifDialogListener positiveClick, String negativeBtnText, FancyGifDialogListener negativeClick) {
        if (!isFinishing()) {
            @SuppressLint("ResourceType") FancyGifDialog fm = new FancyGifDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(msg)
                    .setNegativeBtnText(negativeBtnText)
                    .setPositiveBtnBackground(getResources().getString(R.color.badgecolor))
                    .setPositiveBtnText(positiveBtnText)
                    .setNegativeBtnBackground(getResources().getString(R.color.colorPrimaryDark))
                    .setGifResource(getGifResource(dialog))
                    .isCancellable(true)
                    .OnPositiveClicked(positiveClick)
                    .OnNegativeClicked(negativeClick)
                    .build();

            return fm;
        } else {
            return null;
        }
    }

    private int getGifResource(Dialog_Type dialog) {
        int gifRes = R.drawable.gif1;

        if (dialog == Dialog_Type.FAILURE)
            gifRes = R.drawable.gif_failure;

        else if (dialog == Dialog_Type.SUCCESS)
            gifRes = R.drawable.gif_success;

        else if (dialog == Dialog_Type.LOADING)
            gifRes = R.drawable.gif_loading;

        else if (dialog == Dialog_Type.PROMPT)
            gifRes = R.drawable.gif_prompt;

        return gifRes;
    }

    public void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 300 milliseconds
        long vibration_time = 300;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(vibration_time, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(vibration_time);
        }
    }

    public static void hideKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }
}

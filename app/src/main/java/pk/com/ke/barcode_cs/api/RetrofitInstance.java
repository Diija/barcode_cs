package pk.com.ke.barcode_cs.api;

import com.google.gson.GsonBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pk.com.ke.barcode_cs.Config;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitInstance {
    static Retrofit retrofit;

    public static Retrofit getInstance(){
        if(retrofit == null) {
            //SETTING UP OKHTTP CLIENT
            OkHttpClient okHttpClient;
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(Config.LOG_LEVEL);

            builder.addInterceptor(loggingInterceptor);
            builder.connectTimeout(20, TimeUnit.SECONDS);
            builder.readTimeout(60, TimeUnit.SECONDS);

            X509TrustManager x509TrustManager = null;
            SSLSocketFactory sslSocketFactory = null;
            try {
                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream bytes = new ByteArrayInputStream(getCertificate().getBytes());
                X509Certificate x509Certificate = (X509Certificate) certFactory.generateCertificate(bytes);


                // Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", x509Certificate);


                // Create a TrustManager that trusts the CAs in our KeyStore.
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
                trustManagerFactory.init(keyStore);

                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                x509TrustManager = (X509TrustManager) trustManagers[0];


                // Create an SSLSocketFactory that uses our TrustManager
                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, new TrustManager[]{x509TrustManager}, null);
                sslSocketFactory = sslContext.getSocketFactory();


                //TODO: hostname veridication fix needed

                //create Okhttp client
                builder.sslSocketFactory(sslSocketFactory, x509TrustManager);

                builder.hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
            } catch (CertificateException | KeyStoreException  | IOException |KeyManagementException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }



            okHttpClient = builder.build();

            //CREATING RETROFIT INSTANCE
            retrofit = new Retrofit.Builder().baseUrl(Config.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                            .setLenient()
                            .create()))
                    .build();
        }
        return retrofit;
    }

    public static String getCertificate ()
    {
        return Config.CERTIFICATE;
    }

}

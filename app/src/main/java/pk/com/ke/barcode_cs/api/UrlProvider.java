package pk.com.ke.barcode_cs.api;

import static pk.com.ke.barcode_cs.Config.BASE_URL;

public class UrlProvider {

    public static final String DEPLOTED_PORT_2 = "8090";

    public String getLoginUrl(String username, String password, String imei) {
        return BASE_URL +
                "/sap/opu/odata/sap/ZWM_BARCODE_SERVICE_SRV/TOloginSet(" +
                "ImUserid=%27" + username + "%27," +
                "ImPwd=%27" + password + "%27," +
                "ImImei=%27" + imei + "%27)" +
                "/?$format=json";

    }

    //return URL for OPEN TO Service
    public String getOpenToUrl(String username, String warehousenumber, String storageType) {
        return BASE_URL
                + "/sap/opu/odata/sap/ZWM_BARCODE_SERVICE_SRV/ToitemdataAllOutSet?$format=json&&$filter=WAREHOUSENUMBER%20eq%20%27"
                + warehousenumber + "%27and%20LGTYP%20eq%20%27"
                + storageType + "%27and%20IM_USERNAME%20eq%20%27" + username + "%27";
    }

    //return URL for TO Assignment Service
    public String getToAssignmentUrl(String username, String warehouseNumber, String transferOrder, String toItem) {
        return BASE_URL +
                "/sap/opu/odata/sap/ZWM_BARCODE_SERVICE_SRV/TOitemassignmentSet(" +
                "Lgnum=%27" + warehouseNumber + "%27," +
                "PickBy=%27" + username + "%27," +
                "Tanum=%27" + transferOrder + "%27," +
                "Tapos=%27" + toItem + "%27" +
                ")/?$format=json";
    }

    //return URL for TO Assignment Service
    public String getItemPickingUrl(String warehouseNumber, String pickBy, float qty, String transferOrder, String tOItem, String serielStart, String serielStop, String batchNumber) {
        return BASE_URL +
                "/sap/opu/odata/sap/ZWM_BARCODE_SERVICE_SRV/TOitempickSet(" +
                "Lgnum=%27" + warehouseNumber + "%27," +
                "PickBy=%27" + pickBy + "%27," +
//                "Qty=" + Integer.toString(qty) + ".00m," +
                "Qty=" + String.format("%.3f", qty) + "m," +
                "Tanum=%27" + transferOrder + "%27," +
                "Tapos=%27" + tOItem + "%27," +
                "SerialStart=%27" + serielStart + "%27," +
                "SerialEnd=%27" + serielStop + "%27," +
                "ImBatch=%27" + batchNumber + "%27" + ")/?$format=json";

    }
}


package pk.com.ke.barcode_cs.api.models.pick_item;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class D implements Serializable, Parcelable
{

    @SerializedName("__metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("Lgnum")
    @Expose
    private String warehouseNumber;
    @SerializedName("PickBy")
    @Expose
    private String pickBy;
    @SerializedName("Qty")
    @Expose
    private String qty;
    @SerializedName("Tanum")
    @Expose
    private String transferOrder;
    @SerializedName("Tapos")
    @Expose
    private String toItem;
    @SerializedName("ExCode")
    @Expose
    private String responseCode;
    @SerializedName("ExMessage")
    @Expose
    private String responseMessage;
    @SerializedName("SerialStart")
    @Expose
    private String serialStart;
    @SerializedName("SerialEnd")
    @Expose
    private String serialEnd;

    public final static Creator<D> CREATOR = new Creator<D>() {


        @SuppressWarnings({
            "unchecked"
        })
        public D createFromParcel(Parcel in) {
            return new D(in);
        }

        public D[] newArray(int size) {
            return (new D[size]);
        }

    }
    ;
    private final static long serialVersionUID = 4921791399730498297L;

    protected D(Parcel in) {
        this.metadata = ((Metadata) in.readValue((Metadata.class.getClassLoader())));
        this.warehouseNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.pickBy = ((String) in.readValue((String.class.getClassLoader())));
        this.qty = ((String) in.readValue((String.class.getClassLoader())));
        this.transferOrder = ((String) in.readValue((String.class.getClassLoader())));
        this.toItem = ((String) in.readValue((String.class.getClassLoader())));
        this.responseCode = ((String) in.readValue((String.class.getClassLoader())));
        this.responseMessage = ((String) in.readValue((String.class.getClassLoader())));
        this.serialStart = ((String) in.readValue((String.class.getClassLoader())));
        this.serialEnd = ((String) in.readValue((String.class.getClassLoader())));
    }

    public D() {
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseNumber(String warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public String getPickBy() {
        return pickBy;
    }

    public void setPickBy(String pickBy) {
        this.pickBy = pickBy;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTransferOrder() {
        return transferOrder;
    }

    public void setTransferOrder(String transferOrder) {
        this.transferOrder = transferOrder;
    }

    public String getToItem() {
        return toItem;
    }

    public void setToItem(String toItem) {
        this.toItem = toItem;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getSerialStart() {
        return serialStart;
    }

    public void setSerialStart(String serialStart) {
        this.serialStart = serialStart;
    }

    public String getSerialEnd() {
        return serialEnd;
    }

    public void setSerialEnd(String serialEnd) {
        this.serialEnd = serialEnd;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(metadata);
        dest.writeValue(warehouseNumber);
        dest.writeValue(pickBy);
        dest.writeValue(qty);
        dest.writeValue(transferOrder);
        dest.writeValue(toItem);
        dest.writeValue(responseCode);
        dest.writeValue(responseMessage);
        dest.writeValue(serialStart);
        dest.writeValue(serialEnd);
    }

    public int describeContents() {
        return  0;
    }

}


package pk.com.ke.barcode_cs.api.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class D {

    @SerializedName("__metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("ImImei")
    @Expose
    private String imei;
    @SerializedName("ImPwd")
    @Expose
    private String password;
    @SerializedName("ImUserid")
    @Expose
    private String username;
    @SerializedName("ExCode")
    @Expose
    private String responseCode;
    @SerializedName("ExLgnum")
    @Expose
    private String warehouseNumber;
    @SerializedName("ExLgtyp")
    @Expose
    private String storageType;
    @SerializedName("ExMessage")
    @Expose
    private String exMessage;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getWarehouseNumber() {
        return warehouseNumber;
    }

    public void setWarehouseNumber(String warehouseNumber) {
        this.warehouseNumber = warehouseNumber;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getExMessage() {
        return exMessage;
    }

    public void setExMessage(String exMessage) {
        this.exMessage = exMessage;
    }

}

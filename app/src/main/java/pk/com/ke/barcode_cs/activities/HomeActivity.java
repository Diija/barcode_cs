package pk.com.ke.barcode_cs.activities;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.logger.Logger;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.base.BaseActivity;
import pk.com.ke.barcode_cs.adapter.BaseSectionAdapter;
import pk.com.ke.barcode_cs.adapter.RecyclerAdapter;
import pk.com.ke.barcode_cs.api.ApiInterface;
import pk.com.ke.barcode_cs.api.RetrofitInstance;
import pk.com.ke.barcode_cs.api.UrlProvider;
import pk.com.ke.barcode_cs.api.models.opento.OpenTOModel;
import pk.com.ke.barcode_cs.api.models.opento.Result;
import pk.com.ke.barcode_cs.api.models.toassignment.TOAssignmentModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements BaseSectionAdapter.OnItemClickListener {

    @BindView(R.id.swipeRefresh)
    public SwipeRefreshLayout swipeContainer;

    @BindView(R.id.tvName)
    TextView tvName;

    private RecyclerView recyclerView;
    RecyclerAdapter adap_AssignedTO, adap_AllTO;
    private boolean isFilterAlreadyPressed = false; //true when filtering by assigned only
    public MenuItem /*filterButton,*/reloadButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recyclerViewTO);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        setUserName();

        //swipe to refreash
        swipeContainer.setOnRefreshListener(() -> {
            swipeContainer.setRefreshing(true);
            getOpenTo();
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                R.color.Tint1,
                R.color.Tint2
        );

    }

    private void setUserName() {
        try {
            tvName.setText(getUsername() + " (" + getWareHouseNumber() + ":" + getStorageType() + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getOpenTo();
    }

    private RecyclerAdapter getAdapter(List<Result> results) {

        Comparator<Result> movieComparator = (o1, o2) -> o1.getTransferOrder().compareTo(o2.getTransferOrder());
        Collections.sort(results, movieComparator);

        RecyclerAdapter mSectionedRecyclerAdapter = new RecyclerAdapter(getApplicationContext(), results);

        mSectionedRecyclerAdapter.setOnItemClickListener(this);

        return mSectionedRecyclerAdapter;
    }

    private void getOpenTo() {
        swipeContainer.setRefreshing(true);

//        String serviceUrl = new UrlProvider().getOpenToUrl(getUsername(), getWareHouseNumber(), getStorageType());

        //Don't send Storagetype since we need to show all TOs for a warehouse
        // TODO Update --  Sending Storage Type now, because to show only storage type TOs
        String serviceUrl = new UrlProvider().getOpenToUrl(getUsername(), getWareHouseNumber(), getStorageType());

        ApiInterface apiService =
                RetrofitInstance.getInstance().create(ApiInterface.class);

        Call<OpenTOModel> call;

        try {
            call = apiService.openToService(serviceUrl);

            call.enqueue(new Callback<OpenTOModel>() {
                @Override
                public void onResponse(Call<OpenTOModel> call, Response<OpenTOModel> response) {

                    swipeContainer.setRefreshing(false);

                    if (response.body() != null) {
                        OpenTOModel model = response.body();
                        try {
                            if (model.getD().getResults().get(0).getResponseCode().equals("200")) {
                                log("Open To list Size", model.getD().getResults().size());

                                adap_AssignedTO = getAdapter(filterUsername(model));
                                setAdapterAccordingToFilterMenu();
                                toast("Data Refreshed", ToastType.INFO);
                            } else {

                                toast("Data Refreshed.", ToastType.INFO);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            toast("No data found.", ToastType.INFO);
                        }
                    }else{
                        // Error, try to parse error.
                        String error = "";
                        try {
                            error = response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        toast("Error: " + error, ToastType.ERROR);
                        log("Error: " + error);
                        toast("No data found.", ToastType.INFO);

                    }
                }

                @Override
                public void onFailure(Call<OpenTOModel> call, Throwable t) {

                    swipeContainer.setRefreshing(false);
                    Log.e("Service Error", t.toString());

                    toast("Data could not be retrieved" + t.toString(), ToastType.ERROR);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            swipeContainer.setRefreshing(false);
            toast("Exception: " + e.getMessage(), ToastType.ERROR);
        }
    }

    private void setAdapterAccordingToFilterMenu() {

        recyclerView.setAdapter(adap_AssignedTO);
        adap_AssignedTO.notifyDataChanged();
    }

    private List<Result> filterUsername(OpenTOModel model) {
        if (model != null) {

            List<Result> resultsToReturn = new ArrayList<>();

            List<Result> resultsToFilter = model.getD().getResults();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                resultsToReturn = resultsToFilter.stream()
                        .filter(result -> result.getToUser().equals(getUsername()))
                        .filter(result -> Float.parseFloat(result.getDestActualQuantity()) < Float.parseFloat(result.getDestTargetQuantity()))
                        .collect(Collectors.toList());
            } else {
                for (Result r : resultsToFilter) {
                    if (r.getToUser().equals(getUsername())) {

                        Float picked = 0f;
                        Float total = 0f;
                        try {
                            picked = Float.parseFloat(r.getDestActualQuantity());
                            total = Float.parseFloat(r.getDestTargetQuantity());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        if (picked < total)
                            resultsToReturn.add(r);
                    }
                }
            }

//                dataProvider_Assigned.providedData(resultsToReturn);

//            } else {
//                toast("AssignedToMe interface is null", ToastType.INFO);
//            }
            return resultsToReturn;
        } else {
            toast("Model is null", ToastType.ERROR);
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
//        filterButton = findViewById(R.id.action_filter);

        reloadButton = findViewById(R.id.action_Refresh);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        /*if (id == R.id.action_filter) {
            if (isFilterAlreadyPressed) {
                isFilterAlreadyPressed = false;
                item.setIcon(R.drawable.filter_all);
            } else {
                isFilterAlreadyPressed = true;
                item.setIcon(R.drawable.filter_assigned);
            }
            // Change the state of filter
            toggleFilter();
            return true;
        } else*/
        if (id == R.id.action_logout) {
            logout();
            return true;
        } else if (id == R.id.action_Refresh) {
            getOpenTo();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        removeloginCreadentials();
        intent(Login.class, true);
    }

    private void toggleFilter() {
        setAdapterAccordingToFilterMenu();
    }

    RecyclerAdapter getCurrentAdapter() {
        if (isFilterAlreadyPressed) {
            return adap_AssignedTO;

        } else {
            return adap_AllTO;
        }
    }

    @Override
    public void onItemClicked(Result transferOrder, int position) {
//        Toast.makeText(getActivity(), "Item Clicked:" + movie.getToItem(), Toast.LENGTH_SHORT).show();
        //    FancyToast.makeText(getActivity(),transferOrder.getMaterialDescription(),FancyToast.LENGTH_LONG,FancyToast.SUCCESS,false).show();
        if (transferOrder.getToUser().equals("")) {

//            @SuppressLint("ResourceType") FancyGifDialog fm = new FancyGifDialog.Builder(HomeActivity.this)
//                    .setTitle("Assign Transfer Order Item To me")
//                    .setMessage("Would you like to assign this Transfer Order item to yourself")
//                    .setNegativeBtnText("No")
//                    .setPositiveBtnBackground(getResources().getString(R.color.badgecolor))
//                    .setPositiveBtnText("Yes")
//                    .setNegativeBtnBackground(getResources().getString(R.color.colorPrimaryDark))
//                    .setGifResource(R.drawable.gif1)   //Pass your Gif here
//                    .isCancellable(true)
//                    .OnPositiveClicked(() -> {
//                        makeAssignment(transferOrder, position);
//                        //TODO: what to do if assignement fails
//                        transferOrder.setToUser(getUsername());
//                        getCurrentAdapter().notifyItemChanged(position, transferOrder);
//                        getCurrentAdapter().notifyDataChanged();
//                        Bundle bundle = new Bundle();
//                        bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
//                        intent(ScanActivity.class, bundle, false);
//                    })
//                    .OnNegativeClicked(() -> {
//                    })
//                    .build();

            showDialog("Assign Transfer Order Item To me",
                    "Would you like to assign this Transfer Order item to yourself",
                    Dialog_Type.PROMPT,
                    "Yes",
                    () -> {
                        makeAssignment(transferOrder, position);
                        //TODO: what to do if assignement fails
                        transferOrder.setToUser(getUsername());
                        getCurrentAdapter().notifyItemChanged(position, transferOrder);
                        getCurrentAdapter().notifyDataChanged();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
                        intent(ScanActivity.class, bundle, false);
                    },
                    "No",
                    () -> {
                    });

        } else if (transferOrder.getToUser().equals(getUsername())) {
            //TODO: intent to scanning activity.
            Bundle bundle = new Bundle();
            bundle.putParcelable("transferOrder", Parcels.wrap(transferOrder));
            intent(ScanActivity.class, bundle, false);

        } else if (!transferOrder.getToUser().equals(getUsername())) {
            @SuppressLint("ResourceType") FancyGifDialog fm = new FancyGifDialog.Builder(HomeActivity.this)
                    .setTitle("Transfer Order Item Already Assigned")
                    .setMessage("This Transfer Order Item is already assigned to a different user.")
                    .setPositiveBtnBackground(getResources().getString(R.color.Tint1))
                    .setPositiveBtnText("OK")
                    .setGifResource(R.drawable.gif_success)   //Pass your Gif here
                    .isCancellable(true)
                    .OnNegativeClicked(null)
                    .OnPositiveClicked(() -> {
                    })
                    .build();
        }

    }

    @Override
    public void onSubheaderClicked(int position) {
        if (getCurrentAdapter().isSectionExpanded(getCurrentAdapter().getSectionIndex(position))) {
            getCurrentAdapter().collapseSection(getCurrentAdapter().getSectionIndex(position));
        } else {
            getCurrentAdapter().expandSection(getCurrentAdapter().getSectionIndex(position));
        }
    }

    public void makeAssignment(Result transferOrder, int position) {
        String REQUESTTAG = "asasas";
        assignTo(getUsername(), getWareHouseNumber(),
                transferOrder.getTransferOrder(),
                transferOrder.getToItem(),
                transferOrder,
                position
                , REQUESTTAG);
    }

    public interface FragmentDataProvider {
        void providedData(List<Result> results);
    }

    public interface FragmentItemUpdate {
        void updateItem(Result results, int position, String requestTag);
    }

    public void assignTo(String username, String warehousenumber, String transferOrder, String
            toItem, Result transferOrderitem, int position, String requestTag) {
        //TODO: Switch with actual
        String serviceUrl = new UrlProvider().getToAssignmentUrl(username, warehousenumber, transferOrder, toItem);

        ApiInterface apiService =
                RetrofitInstance.getInstance().create(ApiInterface.class);
        showLoadingDialog();

        Call<TOAssignmentModel> call = apiService.toAssignmentService(serviceUrl);

        call.enqueue(new Callback<TOAssignmentModel>() {
            @Override
            public void onResponse(Call<TOAssignmentModel> call, Response<TOAssignmentModel> response) {
                hideLoadingDialog();
                if (response != null && response.body() != null) {
                    TOAssignmentModel model = response.body();
                    if (model.getD().getResponseCode().equals("200")) {
                        toast(model.getD().getResponseMessage(), ToastType.SUCCESS);
//                        itemUpdater.updateItem();

                    } else {
                        toast(model.getD().getResponseMessage(), ToastType.ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<TOAssignmentModel> call, Throwable t) {
                // Log error here since request failed
                hideLoadingDialog();
                Logger.e("Service Error", t.toString());
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                toast(t.toString(), ToastType.ERROR);

            }
        });
    }


}

package pk.com.ke.barcode_cs.utils;

import android.view.animation.Interpolator;

public class EaseOutElasticInterpolator implements Interpolator {
    private final float mFactor;

    public EaseOutElasticInterpolator() {
        mFactor = 1.0f;
    }

    /**
     * Constructor
     *
     * @param factor Degree to which the animation should be eased. Increasing factor above 1.0f
     *               exaggerates the elasticity and increases the frequency (i.e., it bounces
     *               farther and more times)
     */
    public EaseOutElasticInterpolator(float factor) {
        mFactor = factor;
    }

    @Override
    public float getInterpolation(float input) {
        return (float) (Math.pow(2, -10 * input) * Math.sin(mFactor * 20 * input - (Math.PI / 2)) + 1);
    }
}
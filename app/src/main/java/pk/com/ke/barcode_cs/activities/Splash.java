package pk.com.ke.barcode_cs.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import pk.com.ke.barcode_cs.R;
import pk.com.ke.barcode_cs.activities.base.BaseActivity;

public class Splash extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

        new Handler().postDelayed(() -> {
            if(getUsername() == null || getUsername().equals("")){
                intent(Login.class,true);
            }
            else{
                intent(HomeActivity.class,true);
            }
        }, 1500);
    }
}
